from datetime import datetime
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.models import Variable

args = {
    "owner":"ubuntu",
    "start_date":datetime(2019, 5, 15)
    }


dag = DAG('Courier_Rate', description='Export Courier_Rate DAG',
          # //schedule_interval='0 12 * * *',
          schedule_interval=None,
          default_args=args)

date = Variable.get("DateRange")

spark_operator = SparkSubmitOperator(task_id='submit_task',
        conn_id='my_spark',
        application="/home/ubuntu/csvexporter_2.11-0.1.jar",dag=dag,
        application_args=date,
        java_class='Courier_Rate')

