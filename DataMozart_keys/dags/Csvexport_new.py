from datetime import datetime
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.models import Variable


args = {
    "owner":"ubuntu",
    "start_date":datetime(2019, 5, 15)
    }


dag = DAG('Shipments_Main_loop', description='Export shipments DAG',
          # //schedule_interval='0 12 * * *',
          schedule_interval=None,
          default_args=args)


config = Variable.get("dag_config", deserialize_json=True)

year = config["Year"]
fro = int(config["From"])-1
to = int(config["To"])



task_list = []

for i in range(fro,to):
    task_list.append(SparkSubmitOperator(
        task_id='submit_task'+str(i),
        dag=dag,
		conn_id='my_spark',
		application="/home/ubuntu/csvexporter_2.11-0.1.jar",
		application_args="{y}-{m}-01 - {y}-{m}-02".format(y=year,m=i+1),
		jars="/home/ubuntu/config-1.2.1.jar",
		java_class='csvExporter',
		executor_memory="2G"))
    if i != 0:
        task_list[i-1] >> task_list[i]

