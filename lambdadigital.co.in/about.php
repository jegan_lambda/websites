<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - About Us</title>
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />

<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/revolution-slider.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
    	<?php $activePage  = 'about';  include('header.php');    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>About us</h1>
        </div>
    </section>
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
                    <li>About Us</li>
                </ul>
            </div>
            <div class="pull-right">
               <!-- <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Deafult Section-->
    <section class="default-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Gallery Column-->
                <div class="gallery-column col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                        <div class="row clearfix">
                        	<div class="column col-md-7 col-sm-7 col-xs-12">
                            	<div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-1.jpg" title="Image Caption Here"><img src="./images/resource/gallery-1.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="column col-md-5 col-sm-5 col-xs-12">
                            	<div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-2.jpg" title="Image Caption Here"><img src="./images/resource/gallery-2.jpg" alt="" /></a>
                                </div>
                                <div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-3.jpg" title="Image Caption Here"><img src="./images/resource/gallery-3.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="about-column column welcome col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                        <div class="sec-title">
                            <h2>Welcome to Lambda<span class="theme_color">Digital</span></h2>
                            <div class="separater"></div>
                        </div>
                        <div class="text">
                        	<p>Promoted by professionals with a combined experience of 100+ person years on Global & Domestic Projects.
</p>
                            <p>LambdaDigital provides state-of-the-art software development and support services to clients in India, Asia, Europe and the US since 2017. Completed about 50 person-years of work till date.</p>
							<p>Key Focus Areas - ML/Predictive Analytics, BigData, IoT, Social, Cloud Strategy/Integration, Mobility, Enterprise Applications, DevOps Monitoring & Support.</p>

                        </div>
                        <div class="clearfix">
                        	<div class="author-info pull-left">
                            	<div class="img-thumb">
                                	<img src="./images/resource/author-1.jpg" alt="" />
                                </div>
                                <h4>Saravanan Ananthakrishnan</h4>
                                <div class="designation">CEO & Founder of LambdaDigital</div>
                            </div>
                            <div class="pull-right signature">
                            	<div class="signature-inner">
                                	<img src="./images/resource/signature.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <!--Fun Facts Section-->
            <div class="fact-counter-column">
                <div class="fact-counter">
                    <div class="auto-container">
                        <div class="row clearfix">
                        
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="3500" data-stop="5">5</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Completed Projects</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="1">1</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Years of Experience</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="10">10</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Expert Workers</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="2">2</span>
                                    </div>
                                    <h4 class="counter-title">Clients</h4>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
    <!--End Deafult Section-->
    
    <!--Welcome Section-->
    <section class="welcome-section grey-bg">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<h2>What We Do</h2>
                <div class="text">With our deep technical expertise we will help you fullfill your new business opportunities.</div>
                <div class="separater"></div>
            </div>
            <!--End Sec Title-->
            
            <div class="row clearfix">
            	<!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./technology.php"><img src="./images/resource/welcome-1.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./technology.php">Business Advisor</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./technology.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./outsourcing.php"><img src="./images/resource/welcome-2.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./outsourcing.php">Market Analyze</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./outsourcing.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./consulting.php"><img src="./images/resource/welcome-3.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./consulting.php">Consultation</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./consulting.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>
    <!--End Welcome Section-->
    
    <!--Call To Action Two-->
<!--    <section class="call-to-action-two" style="background-image:url(./images/background/4.jpg);">
    	<div class="auto-container">
        	<h3>We Are The Best</h3>
            <h2>Consulting Company Ever!!</h2>
            <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
        </div>
    </section>-->
    <!--End Call To Action Two-->
    
    <!--Team Section-->
   <!-- <section class="team-section">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">-->
                    <!--Sec Title-->
                  <!--  <div class="sec-title">
                        <h2>Our Experts</h2>
                        <div class="text">We have more than a 10+ experts ready to help you.</div>
                        <div class="separater"></div>
                    </div>
                </div>
                <div class="pull-right">
                	<a href="#" class="theme-btn btn-style-six all-members">See all Members</a>
                </div>
            </div>
            <div class="row clearfix">-->
            	<!--Team Block-->
            <!--	<div class="team-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<img src="./images/resource/team-1.jpg" alt="" />
                            <div class="overlay-box">
                            	<ul class="social-icon-two">
                                	<li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="#">Santhosh</a></h3>
                            <div class="designation">Senior Content Writer</div>
                        </div>
                    </div>
                </div>-->
                
                <!--Team Block-->
            <!--	<div class="team-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<img src="./images/resource/team-2.jpg" alt="" />
                            <div class="overlay-box">
                            	<ul class="social-icon-two">
                                	<li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="#">Naveen</a></h3>
                            <div class="designation">SEO Expert</div>
                        </div>
                    </div>
                </div>-->
                
                <!--Team Block-->
            <!--	<div class="team-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<img src="./images/resource/team-3.jpg" alt="" />
                            <div class="overlay-box">
                            	<ul class="social-icon-two">
                                	<li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="#">Vaithi</a></h3>
                            <div class="designation">Affiliate Marketer</div>
                        </div>
                    </div>
                </div>-->
                
                <!--Team Block-->
          <!--  	<div class="team-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<img src="./images/resource/team-4.jpg" alt="" />
                            <div class="overlay-box">
                            	<ul class="social-icon-two">
                                	<li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="#">Santhakumar</a></h3>
                            <div class="designation">Product Manager</div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>-->
    <!--End Team Section-->
    
    <!--Client Section-->
   <!-- <section class="client-section">
    	<div class="auto-container">-->
        	<!--Sec Title-->
       <!--     <div class="sec-title centered">
            	<div class="sub-title">Testimonials</div>
                <h2>What Clients Say</h2>
                <div class="separater"></div>
            </div>
            <div class="two-item-carousel owl-carousel owl-theme">-->
            	<!--Client Box-->
            	<!--<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box">
                            	<span class="icon fa fa-facebook"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Santhosh.</h3>
                        <div class="designation">Web Specialist</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>
-->                
                <!--Client Box-->
            <!--	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box instagram">
                            	<span class="icon fa fa-instagram"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Naveen</h3>
                        <div class="designation">Integration Specialist</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>
                -->
                <!--Client Box-->
            	<!--<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box">
                            	<span class="icon fa fa-facebook"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Santhosh.</h3>
                        <div class="designation">Web Specialist</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>
                -->
                <!--Client Box-->
            <!--	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box instagram">
                            	<span class="icon fa fa-instagram"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Naveen</h3>
                        <div class="designation">Integration Specialist</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>-->
    <!--End Client Section-->
    
    <!--Sponsors Section-->
    <section class="sponsors-section">
    	<div class="auto-container">
        	<div class="carousel-outer">
                <!--Sponsors Slider-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_1.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_2.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_3.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_4.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_5.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_6.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_7.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_8.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_9.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_10.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_1.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_2.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_3.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_4.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_5.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_6.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_7.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_8.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_9.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_10.png" alt="" /></a></div></li>
                </ul>
            </div>
    	</div>
    </section>
    <!--End Sponsors Section-->
    
    <!--Main Footer-->
   <?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/revolution.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
