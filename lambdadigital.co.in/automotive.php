<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - A technology & business consulting company | Project Two</title>
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
	<?php   include_once "header.php";    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Project Gallery</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
                    <li>Industries</li>
					 <li>Automotive</li>
                </ul>
            </div>
            <div class="pull-right">
               <!-- <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Gallery Section-->
    <section class="gallery-section">
    	<div class="auto-container">
        	
            <!--Sortable Gallery-->
            <div class="mixitup-gallery">
            
                <!--Filter-->
                <div class="filters clearfix">
                    <ul class="filter-tabs filter-btns">
                        <li class="filter active" data-role="button" data-filter="all">All</li>
                        <li class="filter" data-role="button" data-filter=".research">Market Research</li>
                        <li class="filter" data-role="button" data-filter=".advice">Financial Advise</li>
                        <li class="filter" data-role="button" data-filter=".invest">Investmane</li>
                        <li class="filter" data-role="button" data-filter=".account">Accounting</li>
                        <li class="filter" data-role="button" data-filter=".sales">Sales & Trading</li>
                    </ul>
                </div>
                
                <div class="filter-list row clearfix">
                
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix all col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/11.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/11.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Market Research & Growth</a></h3>
                                <div class="designation">Account, Finance, Invest</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix sales invest col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/12.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/12.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Financial Advise</a></h3>
                                <div class="designation">Finance, Advise</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix research invest account col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/13.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/13.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Consumer Products</a></h3>
                                <div class="designation">Cash, sales & Trading</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix advice research col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/14.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/14.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Sales & Marketing</a></h3>
                                <div class="designation">Sales, Invest</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix account research col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/15.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/15.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Banking Investment</a></h3>
                                <div class="designation">Invest, Account</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Gallery Block Two-->
                    <div class="gallery-block-two mix invest col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                            	<img src="./images/gallery/16.jpg" alt="" />
                                <div class="overlay-box">
                                	<div class="overlay-inner">
                                        <div class="content">
                                            <a href="./project-single.html" class="icon"><span class="fa fa-link"></span></a>
                                            <a href="./images/gallery/16.jpg" class="icon image-link lightbox-image" data-fancybox-group="example-gallery" title=""><span class="fa fa-search"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </figure>
                            <div class="lower-box">
                            	<h3><a href="./project-single.html">Business Strategies</a></h3>
                                <div class="designation">Business, Market, Sales</div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <!--End Sortable Gallery-->
            
        </div>
    </section>
    <!--Gallery Section-->
    
    <!--Main Footer-->
    <?php   include_once "footer.php";    ?>

<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/mixitup.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
