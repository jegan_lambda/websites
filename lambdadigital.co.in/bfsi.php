<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - A technology & business consulting company | Project Single</title>
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
	<?php   include_once "header.php";    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Single Project</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
                    <li>Project</li>
                </ul>
            </div>
            <div class="pull-right">
                <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Sidebar Category -->
                        <div class="sidebar-widget sidebar-category">
                            <ul class="list">
                                <li class="current"><a href="./project-single.html">Mobility</a></li>
                                <li><a href="./cloud.html">Cloud Migration</a></li>
                                <li><a href="./ml.html">ML/Predictive Analytics</a></li>
                                <li><a href="./bpm.html">BPM/EAI</a></li>
                                <li><a href="./social.html">Social Applications</a></li>
                                <li><a href="./iot.html">IoT</a></li>
                            </ul>
                        </div>
                        
                        <!-- Sidebar Testimonial -->
                        <div class="sidebar-widget sidebar-testimonial">
                        	<div class="single-item-carousel owl-carousel owl-theme">
                            	<!--Testimonial Slide-->
                            	<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                                <!--Testimonial Slide-->
                            	<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                                <!--Testimonial Slide-->
                            	<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!-- Sidebar Question Box -->
                        <div class="sidebar-widget sidebar-question">
                        	<div class="info-widget">
                                <div class="inner">
                                    <h3>Have Any Question <br /> Call Us Now</h3>
                                    <h2>+91 9840 358 348</h2>
                                    <a href="./contact.html" class="more-detail">More Details</a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Sidebar brochure-->
                        <div class="sidebar-widget sidebar-brochure">
                        	<a class="brochure" href="#"><span class="icon flaticon-pdf"></span> Details Brochure.pdf <span class="download-icon flaticon-download-arrow-1"></span></a>
                        </div>
                        
                    </aside>
                </div>
                
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<!--Content Side / PROJECT Single-->
                	<div class="project-single">
						<div class="project-single-images">
                        	<div class="row clearfix">
                            	<div class="column col-md-8 col-sm-8 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/project-1.jpg" title="Image Caption Here"><img src="./images/resource/project-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/project-2.jpg" title="Image Caption Here"><img src="./images/resource/project-2.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Upper Box-->
                        <div class="upper-box">
                        	<h2>Business & Marketing Growth</h2>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box">
                        	<div class="text">
                            	<p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Ei mundi solet ut soletu mel possit quo. Sea cu justo laudem. An utinam consulatu eos, est facilis suscipiantur te, vim te iudico delenit voluptatibus. Te eos accusam repudiandae eros.</p>
                                <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment. Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation.</p>
                            </div>
                            <div class="row clearfix">
                            	<div class="column col-md-5 col-sm-5 col-xs-12">
                                	<div class="image">
                                    	<img src="./images/resource/project-3.jpg" alt="" />
                                    </div>
                                </div>
                                <div class="column col-md-7 col-sm-7 col-xs-12">
                                	<h3>Project Details</h3>
                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumq. Temu nec dolor clita partem mea ne iuvaret aliquid. Id namillum aug commodo, diam dolores philosophia. </p>
                                    <h4>What was included in the project</h4>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<!--List Style Three-->
                                        	<ul class="list-style-three">
                                            	<li>Date : <span>12 Mar, 2016</span></li>
                                                <li>Location : <span>New Yrok, NY</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<!--List Style Three-->
                                        	<ul class="list-style-three">
                                            	<li>Categories : <span>Finance & Legal</span></li>
                                                <li>Tag: <span>Financial Advise.</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Graph Block-->
                            <div class="graph-block">
                            	<h3>Project Analysis</h3>
                                <!--Graph-->
                            	<div class="graph-outer">
                                    <canvas id="analysis-graph" style="width:100%;"></canvas>
                                </div>
                                <div class="stats-box clearfix">
                                	<div class="pull-left">
                                        <ul class="clearfix">
                                            <li><strong>385</strong>Total Visitors</li>
                                            <li><strong>422</strong>Sessions</li>
                                            <li><strong>53%</strong>Bounce Rate</li>
                                            <li><strong>29</strong>Conversions</li>
                                        </ul>
                                    </div>
                                    
                                    <div class="pull-right">
                                        <a href="#" class="theme-btn btn-style-four">Incoming Statstics</a>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Project Solution Section-->
                            <div class="project-solution-section">
                            	<h3>Project Solution</h3>
                                
                                <div class="product-info-tabs">
                                    <!--Product Tabs-->
                                    <div class="prod-tabs tabs-box" id="product-tabs">
                                    
                                        <!--Tab Btns-->
                                        <ul class="tab-btns tab-buttons clearfix">
                                            <li data-tab="#prod-approch" class="tab-btn active-btn">Our Approch</li>
                                            <li data-tab="#prod-chalange" class="tab-btn">Challange</li>
                                            <li data-tab="#prod-result" class="tab-btn">Solution & Result</li>
                                        </ul>
                                        
                                        <!--Tabs Content-->
                                        <div class="tabs-container tabs-content">
                                            
                                            <!--Tab / Active Tab-->
                                            <div class="tab active-tab" id="prod-approch">
                                                <ul class="list-box">
                                                	<li><span>First Step:</span> Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Ei mundi solet ut soletu mel possit quo. Sea cu justo laudem. An utinam consulatu eos, est facilis suscipiantur te, vim te iudico delenit voluptatibus. Te eos accusam repudiandae eros.</li>
                                                    <li><span>Second Step:</span> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</li>
                                                    <li><span>Third Step:</span>  Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment. Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation.</li>
                                                </ul>
                                            </div>
                                            
                                            <!--Tab-->
                                            <div class="tab" id="prod-chalange">
                                                <ul class="list-box">
                                                	<li><span>Third Step:</span>  Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment. Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation.</li>
                                                	<li><span>First Step:</span> Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Ei mundi solet ut soletu mel possit quo. Sea cu justo laudem. An utinam consulatu eos, est facilis suscipiantur te, vim te iudico delenit voluptatibus. Te eos accusam repudiandae eros.</li>
                                                    <li><span>Second Step:</span> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</li>
                                                </ul>
                                            </div>
                                            
                                            <!--Tab -->
                                            <div class="tab" id="prod-result">
                                                <div class="content">
                                                    <ul class="list-box">
                                                        <li><span>First Step:</span> Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Ei mundi solet ut soletu mel possit quo. Sea cu justo laudem. An utinam consulatu eos, est facilis suscipiantur te, vim te iudico delenit voluptatibus. Te eos accusam repudiandae eros.</li>
                                                        <li><span>Third Step:</span>  Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment. Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation.</li>
                                                        <li><span>Second Step:</span> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
	<?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>

<!--Analysis Graph Script-->
<script src="./js/chart.js"></script>
<script src="./js/graph.js"></script>

</body>
</html>
