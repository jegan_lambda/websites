<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Careers</title
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/revolution-slider.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body onload="initialize()">
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
    	<?php $activePage  = 'about';  include('header.php');    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Careers</h1>
        </div>
    </section>
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
                    <li>Careers</li>
                </ul>
            </div>
            <div class="pull-right">
               <!-- <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Deafult Section-->
    <section class="default-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Gallery Column-->
                <div class="gallery-column col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                        <div class="row clearfix">
                        	<div class="column col-md-7 col-sm-7 col-xs-12">
                            	<div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-1.jpg" title="Image Caption Here"><img src="./images/resource/gallery-1.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="column col-md-5 col-sm-5 col-xs-12">
                            	<div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-2.jpg" title="Image Caption Here"><img src="./images/resource/gallery-2.jpg" alt="" /></a>
                                </div>
                                <div class="image">
                                	<a class="lightbox-image" href="./images/resource/gallery-3.jpg" title="Image Caption Here"><img src="./images/resource/gallery-3.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="about-column column welcome col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                        <div class="sec-title">
                            <h2>Welcome to Lambda<span class="theme_color">Digital</span></h2>
                            <div class="separater"></div>
                        </div>
                        <div class="text">
                        	<p>LambdaDigital was started with a bold vision to make a significant contribution to the business world by providing prudent information system solutions.</p>
                            <p>We listen carefully to understand our clients' needs and work with our clients and their partners to create meaningful solutions that improve our clients' business performance. We trust in empowering professionals to efficiently collaborate and quickly achieve the goal. We pursue alliances that further our organization.</p>
							<p>We constantly acquire key abilities and hone to be the forte. We set organizational road map for competence building. We create an open environment, for our professionals, designed to stimulate the flow of information among the team. By joining LambdaDigital, you will be part of technologically thriving professionals.</p>
							<p>We are in INDIA (Chitlapakkam, Chennai).</p>
                        </div>
                        <!--<div class="clearfix">
                        	<div class="author-info pull-left">
                            	<div class="img-thumb">
                                	<img src="./images/resource/author-1.jpg" alt="" />
                                </div>
                                <h4>Saravanan Ananthakrishnan</h4>
                                <div class="designation">CEO & Founder of LambdaDigital</div>
                            </div>
                            <div class="pull-right signature">
                            	<div class="signature-inner">
                                	<img src="./images/resource/signature.png" alt="" />
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                
            </div>
           
        </div>
    </section>
    <!--End Deafult Section-->
    
    
    
    <!--Main Footer-->
   <?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/revolution.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>

