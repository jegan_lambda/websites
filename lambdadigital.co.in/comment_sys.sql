-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2017 at 01:02 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `comment_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `comment` text NOT NULL,
  `id_post` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `comment`, `id_post`, `date`) VALUES
(118, 'Guest', '', 'water@gade.come', 1, '2017-09-07 13:41:15'),
(119, 'Guest', '', 'water@gade.come', 1, '2017-09-07 13:41:24'),
(120, 'sfs', 'n.sahtos@gmail.com', 'sdfsd', 1, '2017-09-07 13:42:25'),
(121, 'sdffs', 'sdfs', 'sdfs', 1, '2017-09-07 13:42:34'),
(122, 'ertet', 'sdfsdfs', 'sdfsf', 1, '2017-09-07 13:43:22'),
(123, 'eawfasef', 'awer@gmail.com', 'sdf', 1, '2017-09-07 13:44:52'),
(124, 'kl;sadkl;', 'awer@gmail.com', 'dsfjkl;sd', 1, '2017-09-07 13:45:18'),
(125, 'aweraw', 'ert@gmailc.om', 'fds', 1, '2017-09-07 13:46:16'),
(126, 'sdfs', 'n.santhosh@gmail.com', 'dsfssdfds', 1, '2017-09-07 13:46:36'),
(127, 'dsfsd', 'sdsdewa@gmail.com', 'sdfsdf', 1, '2017-09-07 13:47:25'),
(128, 'dfsf', 'sdfsd@gmail.com', 'adfsfs', 1, '2017-09-07 13:48:14'),
(129, 'awer', 'awrewaer@gamil.com', 'awer', 1, '2017-09-07 13:52:17'),
(130, 'awera', 'n.sahtos@gmail.com', 'awerawe', 1, '2017-09-07 13:53:11'),
(131, 'santhosh', 'n.santhosh@gmail.com', 'tesfsdf', 1, '2017-09-07 13:54:03'),
(132, 'santhsoh', 'n.santhosh@gmail.com', 'this comment', 1, '2017-09-07 13:54:40'),
(133, 'awer', 'awer', 'arweraw', 1, '2017-09-07 13:56:11'),
(134, 'fsdf', 'sdfsd@gmail.com', 'sdfs', 1, '2017-09-07 13:57:03'),
(135, 'awer', 'n.sahtos@gmail.com', 'fsdf', 1, '2017-09-07 13:57:35'),
(136, 'Guest', '', 'sdfsd', 1, '2017-09-07 14:09:20'),
(137, 'Guest', '', 'fds', 1, '2017-09-07 14:09:32'),
(138, 'sd', 'sdfsd@gmail.com', 'wer', 1, '2017-09-07 14:11:40'),
(139, 'wer', 'n.sahtos@gmail.com', 'afds', 1, '2017-09-07 14:13:27'),
(140, 'santhsoh', 'bsanthoshmca#gmai.com', 'jsdljfldsfds', 1, '2017-09-07 14:15:05'),
(141, 'nsldkljfl', 'sdfsdfsdf@gmail.com', 'sdfsfs', 1, '2017-09-07 14:15:55'),
(142, 'sss', 'sss@test.com', 'adasl;jdjsaljl', 1, '2017-09-07 17:01:39'),
(143, 'FDAFDSF', 'TEST1@TEST.COM', 'SADASDD', 1, '2017-09-07 17:02:01'),
(144, 'SSS', '', 'DSDADA', 65500, '2017-09-07 17:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `threaded_comments`
--

CREATE TABLE `threaded_comments` (
  `comment_id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threaded_comments`
--

INSERT INTO `threaded_comments` (`comment_id`, `author`, `comment`, `created_at`, `parent_id`) VALUES
(24, 'santhosh', 'aweraw', '2017-09-08 06:12:57', 0),
(23, 'waer', 'wer', '2017-09-08 04:57:01', 21),
(22, 'awera', 'awer', '2017-09-08 04:56:25', 0),
(21, 'awer', 'awr', '2017-09-08 04:56:23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `threaded_comments2`
--

CREATE TABLE `threaded_comments2` (
  `comment_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threaded_comments2`
--

INSERT INTO `threaded_comments2` (`comment_id`, `name`, `email`, `comment`, `created_at`, `parent_id`) VALUES
(63, 'santhosh', 'n.santhoshmca@gmail.com', 'tear', '2017-09-08 06:56:40', 0),
(62, 'santhosh', 'n.santhoshmca@gmail.com', 'test', '2017-09-08 06:52:54', 59),
(61, 'santhosh', 'n.santhoshmca@gmail.com', 'sa', '2017-09-08 06:24:45', 0),
(60, 'santhosh', 'n.santhoshmca@gmail.com', 'waer', '2017-09-08 06:18:14', 0),
(59, 'rushia', 'n.santhoshmca@gmail.com', 'werawr', '2017-09-08 06:17:04', 55),
(57, 'erwe', 'n.santhoshmca@gmail.com', 'df', '2017-09-08 06:14:57', 0),
(58, 'sarvar', 'sarva@gmail.com', 'test gdfgdfgdfg', '2017-09-08 06:15:22', 23),
(56, 'karthi', 'n.santhoshmca@gmail.com', 'secon command', '2017-09-08 06:11:01', 0),
(55, 'arun', 'arun@mgail.com', 'test', '2017-09-08 06:10:20', 54),
(54, 'santhosh', 'n.santhoshmca@gmail.com', 'tserewa', '2017-09-08 06:08:07', 0),
(64, 'vigneash', 'vignesh@gmail.com', 'hellow ', '2017-09-08 06:58:16', 60),
(65, 'santhosh', 'bsa@gmail.com', 'fsdf', '2017-09-08 06:59:39', 0),
(66, 'santhosh', 'n.santhoshmca@gmail.com', 'teste', '2017-09-08 07:00:22', 0),
(67, 'santhosh', 'n.santhoshmca@gmail.com', 'wear', '2017-09-08 07:00:51', 0),
(68, 'santhosh', 'n.santhosh@gmail.com', 'tes', '2017-09-08 07:01:53', 0),
(69, 'santhosh', 'n.santhoshmca@gmail.com', 'ateaw', '2017-09-08 07:08:48', 0),
(70, 'santhosh', 'n.santhoshmca@gmail.com', 'tsd', '2017-09-08 07:30:05', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threaded_comments`
--
ALTER TABLE `threaded_comments`
  ADD UNIQUE KEY `uid` (`comment_id`);

--
-- Indexes for table `threaded_comments2`
--
ALTER TABLE `threaded_comments2`
  ADD UNIQUE KEY `uid` (`comment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `threaded_comments`
--
ALTER TABLE `threaded_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `threaded_comments2`
--
ALTER TABLE `threaded_comments2`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
