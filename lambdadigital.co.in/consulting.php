<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Consulting</title>
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
<?php   include_once "header.php"; ?>
    <!--End Main Header -->
    
	<!--Page Title-->
	<div id="pg_tl_data_analytics"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Data Analytics</h1>
			</div>
		</section>
	</div>	

	<div style="display:none;" id="pg_tl_it_strategy_consul"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>IT Strategy & Consulting</h1>
			</div>
		</section>
	</div>	

	<div style="display:none;" id="pg_tl_cloud_strategy"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Cloud Strategy</h1>
			</div>
		</section>
	</div>	

	<div style="display:none;" id="pg_tl_digital_engg"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Digital Engineering</h1>
			</div>
		</section>
	</div>	

	<div style="display:none;" id="pg_tl_it_security"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>IT Security</h1>
			</div>
		</section>
	</div>	
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
					<li>Services</li>
                    <li>Consulting</li>
                </ul>
            </div>
            <div class="pull-right">
              <!--  <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Sidebar Category -->
                        <div class="sidebar-widget sidebar-category">
                            <ul class="list">
                                <li class="current" id="left_menu_data_analystics"><a href="javascript:show('data_analytics'); javascript:display('pg_tl_data_analytics','left_menu_data_analystics');">Data Analytics</a></li>
                                <li id="left_menu_it_strategy"><a href="javascript:show('it_strategy_consul'); javascript:display('pg_tl_it_strategy_consul','left_menu_it_strategy');">IT Strategy & Consulting</a></li>
                                <li id="left_menu_cloud_strategy"><a href="javascript:show('cloud_strategy'); javascript:display('pg_tl_cloud_strategy','left_menu_cloud_strategy');">Cloud Strategy & Migration</a></li>
                                <li id="left_menu_digi_eng"><a href="javascript:show('digital_engg'); javascript:display('pg_tl_digital_engg','left_menu_digi_eng');">Digital Engineering</a></li>
                                <li id="left_menu_info_security"> <a href="javascript:show('it_security'); javascript:display('pg_tl_it_security','left_menu_info_security');">Information Security</a></li>
                            </ul>
                        </div>
                        
                        <!-- Sidebar Testimonial -->
                       <!-- <div class="sidebar-widget sidebar-testimonial">
                        	<div class="single-item-carousel owl-carousel owl-theme">
                            	<!--Testimonial Slide-->
                            	<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>-->
                                
                                <!--Testimonial Slide-->
                            <!--	<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                                <!--Testimonial Slide-->
                            	<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>-->
                        
                        <!-- Sidebar Question Box -->
                        <div class="sidebar-widget sidebar-question">
                        	<div class="info-widget">
                                <div class="inner">
                                    <h3>Have Any Question <br /> Call Us Now</h3>
                                    <h2>+91 9840 358 348</h2>
                                    <a href="#" class="more-detail">More Details</a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Sidebar brochure-->
                        <div class="sidebar-widget sidebar-brochure">
                        	 <!--<a class="brochure" href="#"><span class="icon flaticon-pdf"></span> Details Brochure.pdf <span class="download-icon flaticon-download-arrow-1"></span></a>-->
                        </div>
                        
                    </aside>
                </div>
                
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<!--Services Single-->
                	<div class="services-single">
						<div class="services-single-images">
                        	<div class="row clearfix">
                            	<div class="column col-md-8 col-sm-8 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-1.jpg" title="Image Caption Here"><img src="./images/resource/services-single-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-2.jpg" title="Image Caption Here"><img src="./images/resource/services-single-2.jpg" alt="" /></a>
                                    </div>
                                    <div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-3.jpg" title="Image Caption Here"><img src="./images/resource/services-single-3.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                      
             <!--Data Analytics Block Start-->          
             <div  id="data_analytics"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Data Analytics</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>LambdaDigital specializes in Data collection, Data pre-processing, Visualization and ML/Predictive analytics.</p>
                                <p>LambdaDigital team has deep expertise on data Visualization frameworks namely d3.js, dc.js, crossfilter.js, topojson.js, GIS tools (QGIS, ogr2ogr, PostGIS), Inkscape, GIMP for image processing and data analytics tools & platforms like python scikit learn, statsmodels, data preprocessing libraries like patsy, pandas, numpy, scipy, R studio etc.</p>
				<p>LambdaDigital has developed a ML platform that helps customers leverage Technology and Analytics through Interactive Visualization, Geospatial and Location intelligence, Localized Market Understanding for empowering their Marketing, Sales and Product Strategies for a given Location within the pertaining geo contexts built into the platform against relevant econometric data and his own operational data. This platform also enables a customer to evaluate his performance in the context of the market he/she operates in order to avail better planning and decision making.</p>
				<p>LambdaDigital has an enterprise class runtime environment for large-scale data processing, and predictive analytics using Apache Spark, Hadoop, HBASE, Postgres, Rserve, OpenCPU and deployR.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
						<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
						<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Data Analytics  Block End-->   
             
                        
      <!--IT Strategy & Consulting Block Start-->          
                                          
         <div style="display:none;" id="it_strategy_consul"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>IT Strategy & Consulting</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>LambdaDigital provides insight and thought leadership across a range of disciplines, that include IT management and audit, cost optimization, quality control, governance, infrastructure, security and project management.</p>
                                <p>Every organization embarking on a business transformation journey needs a guiding framework that aligns new and existing business applications with an overall IT strategy and ensures businesses are in line with their business objectives.
								Don’t you think you need an IT strategy before you take IT investment decisions?
								</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
						<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
						<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
					<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--IT Strategy & Consulting  Block End-->   
             
      
       <!--Cloud Strategy & Migration Block Start-->          
                                          
         <div style="display:none;" id="cloud_strategy"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Cloud Strategy & Migration</h3>
                            <div class="sub-title">Applications assessment and <span class="theme_color"> Cloud Migration strategy</span></div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>LambdaDigital specializes in optimizing application service delivery in the age of cloud and distributed computing. Leverage our service offering to keep pace with the evolving technology trends and digital economy by transferring your applications and services to the cloud.
								</p>
                                <p>Leveraging API management platforms, cloud infrastructures or managed services in a state-of-the-art data center is key to improving your organization's performance and success. Our Application assessment framework and Cloud Migration methodology provides you with a cost-effective way to prepare yourself for your IT strategy and cloud migration. The Assessment tool identifies applications that are best suited to on-premise, cloud (private, public or hybrid), finds the optimum deployment model for each application and service, and helps plan and execute the next steps.
								</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
						<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
						<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Understand your Business Objectives</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Agility may mean application access and mobility, scalability for fluctuating seasonal or unpredictable user traffic, or application DevOps testing.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Choose the Right Cloud Option</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Select one of the three different hosting offerings: public cloud, private cloud, or hybrid cloud.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Select the Right Technologies</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>You need the right infrastructure, security and service, right automation tools etc. to support your cloud hosting needs. </p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Cloud Strategy & Migration Block End-->         
             
             
			 <!--Digital Engineering Block Start-->          
                                          
			 <div style="display:none;" id="digital_engg"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Digital Engineering</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>Our team uses analytics to discover opportunities, manage risk, reduce fraud and help business focus on solving business problems that analytics can address.</p>
                                <p>We manage and automate the process of data collection, preparation and analysis, and integrate them into the operations. We provide dashboards and reports that provide appropriate and well-timed insights that allow our clients to tactical and strategic decisions that enhance shareholder value. We provide data-driven tools that enable our clients to communicate and collaborate with internal and external constituents while facilitating decision making. Our actionable insights provide clients with a way to monetize the data and transform operational performance to give significant competitive advantage. Our value lies in embedding analytics deeply into business process in a seamless manner.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
						<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
						<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Cloud Strategy & Migration Block End-->     
             
             <!--Information Security Block Start-->          
                                          
			 <div style="display:none;" id="it_security"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Information Security</h3>
                            <div class="sub-title">We help organizations identify risks, validate existing controls, and measure <span class="theme_color">information security posture</span>.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>Most businesses often get targeted and hit by viruses, malware and Ransomware. Make sure you’re prepared with enterprise security and backup solutions.</p>
                                <p>IT Security is a key aspect of your company’s IT Infrastructure. Hackers keep trying to break into public & private networks around the globe to steal information or worse: hold you to ransom.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our priority is your IT security</h2>
                                    <div class="text">Using LambdaDigital Team’s in-depth security knowledge and wealth of experience, we’ll find you the right solution within hours. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Network Access Controls</li>
                                                <li>Intrusion Detection/Prevention</li>
						<li>Email Security</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>End Point Encryption</li>
                                                <li>Vulnerability Mgmt</li>
						<li>Security Event and Info Mgmt</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Assessment Services</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Network Security Assessment</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>System complexity and attack surfaces continue to grow. Perform network-based penetration testing in a structured manner.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Application Security Assessment</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>An unified solution for identifying, securing and monitoring web, mobile applications from development to production.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Information Systems Audit</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Assist enterprises in preparing a comprehensible audit report that complies with requirements of IS Audit, Assurance Standards and Assurance Guidelines that are published by ISACA.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">4</span> </div>Penetration Testing</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Includes network penetration testing and application security testing as well as controls and processes around the networks and applications.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Information Security Block End-->     
                            
                            
                            <!--Consult Box-->
                          <!--<div class="lower-box" >  
                            <div class="consult-box">
                            	<div class="clearfix">
                                	<div class="column pull-left">
                                    	<h3>We Have Great Advisor!!</h3>
                                        <div class="text">Here is something that is related with <span class="theme_color">technology & business</span> consulting services</div>
                                    </div>
                                    <div class="column pull-right">
                                    	<a href="./contact.php" class="theme-btn btn-style-eight">Free Consultation</a>
                                    </div>
                                </div>
                            </div>-->
                            <!--End Consult Box-->
                            
                            
                            
                            <!--div class="accordian-boxed">
                            	<h2>Frequently Asked Question</h2><br />
                            	<Accordion Box>
                                <ul class="accordion-box">
                                
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How i get the free consulting and start with advisor?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How investment plan work for us?</div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How can i make profit with investmant plan? </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                </ul>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
  <?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script type="text/javascript">

function show(divId) {
	$("#data_analytics").hide();
	$("#it_strategy_consul").hide();
	$("#cloud_strategy").hide();
	$("#digital_engg").hide();
	$("#it_security").hide();
	$("#"+divId).show();
}

function display(divId,leftMenuId) {
	$("#pg_tl_data_analytics").hide();
	$("#pg_tl_it_strategy_consul").hide();
	$("#pg_tl_cloud_strategy").hide();
	$("#pg_tl_digital_engg").hide();
	$("#pg_tl_it_security").hide();
	
	$("#left_menu_data_analystics").removeClass("current");
	$("#left_menu_it_strategy").removeClass("current");
	$("#left_menu_cloud_strategy").removeClass("current");
	$("#left_menu_digi_eng").removeClass("current");
	$("#left_menu_info_security").removeClass("current");
	 
	$("#"+leftMenuId).addClass("current");
	$("#"+divId).show();
}

</script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
