<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Contact Us</title
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />

<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/revolution-slider.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body onload="initialize()">


<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
   <?php   include_once "header.php";    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Contact Us</h1>
        </div>
    </section>
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
            <div class="pull-right">
                <!--<div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Contact Section-->
    <section class="contact-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Contact Info Box-->
                <div class="contact-info-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-location-pin"></span>
                        </div>
                        <div class="info-content">
                        	<h3>Address</h3>
                            <div class="text">No.4, 2nd Floor, Kamaraj Colony, Chitlapakkam, Chennai, TN-600064, IND</div>
                        </div>
                    </div>
                </div>
                <!--Contact Info Box-->
                <div class="contact-info-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-business-2"></span>
                        </div>
                        <div class="info-content">
                        	<h3>Phone & Email</h3>
                            <div class="text">(+91) 9840358348 <br /> sales@lambdadigital.co.in</div>
                        </div>
                    </div>
                </div>
                <!--Contact Info Box-->
                <div class="contact-info-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-share"></span>
                        </div>
                        <div class="info-content">
                        	<h3>Stay In Touch</h3>
                            <div class="text">Also find us on social Media</div>
                            <div class="social-icon-four">
                            	<a href="https://www.facebook.com/Lambdadigital-Pvt-Ltd-549625572062402/"><span class="fa fa-facebook-f"></span></a>
                                <a href="https://twitter.com/LambdaDigital"><span class="fa fa-twitter"></span></a>
                                <!--<a href="#"><span class="fa fa-google-plus"></span></a>-->
                                <a href="https://www.linkedin.com/company/lambdadigital/?lipi=urn%3Ali%3Apage%3Ad_flagship3_company_admin%3Bfgr4KtIBR9%2B2migC%2BAF4vg%3D%3D"><span class="fa fa-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            
            
            
            <!--Contact Form Section-->
            <div class="contact-form-section" id="formbox">
            	<div class="row clearfix">
                	<div class="column col-md-7 col-sm-12 col-xs-12">
                    	<h2>Contact Us</h2>
                       
                        <!--Contact Form-->
                        <div class="contact-form" >
                            <form autocomplete="off" name="contact_form" action="contact.php" method="post">
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="first_name"  id="first_name" value="" placeholder="First Name *" required="" />
                                    </div>
                                    
                                    
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="last_name"  id="last_name"  value="" placeholder="Last Name" />
                                    </div>
                                  
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <input type="email" name="user_email"  id="user_email" value="" placeholder="Email *" required="" />
                                    </div>
                                    
									<div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="phone"  id="phone" value="" placeholder="Phone *" required="" />
                                    </div>
									
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="user_message"   id="user_message" placeholder="Message *"></textarea>
                                    </div>
                                    
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                                    	<button type="submit" name="submit" class="theme-btn btn-style-two submit">Send Message</button>
                                    </div>

                                    <?php 
									if(isset($_REQUEST['user_email'])){
										$first_name = $_POST['first_name'];
										$last_name = $_POST['last_name'];
										$email = $_POST['user_email'];
										$phone = $_POST['phone'];
										$subhead = 'Mail from website contact us page';
										$comments = $_POST['user_message'];
										$email_from = $email;
										$email_to = 'sales@lambdadigital.co.in';
										//'booking@shrisubhamresidency.com, shrisubhamresidency@gmail.com';//replace with your email
										$body = 'Name: ' . $first_name . $last_name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Mobile No: ' . $phone . "\n\n". 'Special Request: ' . $comments;
										$subject = $subhead;
										//$headers .='X-Mailer: PHP/' . phpversion();
										//$headers .= "MIME-Version: 1.0\r\n";
										//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
										$success = mail($email_to, $subject, $body, 'From: <'.$email_from.'>');
										$message = "Mail Sent Succesfully";
										echo "<script type='text/javascript'>alert('$message');</script>";
									}
									?>
                                    
                                </div>
                            </form>
                        </div>
                        <!--End Contact Form-->
                    </div>
                    <!--Column-->
                    <div class="column col-md-5 col-sm-12 col-xs-12">
                    	<div class="contact-content">
                        	<h3>Do you <br /> Have Any Question <br /> About Us?</h3>
                            <div class="text">
                            	<p>Any kind of technology solution or consultation don't hesitate to contact us for immeditate customer support. We would love to hear from you</p>
                                <p><span>Office Hours:</span> We are always open except Saturday & <br /> Sunday from <span>10:00am</span> to <span>7:00pm</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Contact Form Section-->
            
        </div>
    </section>
    <!--End Contact Section-->
    
    <!--Map Section-->
    <section class="map-section">
        <div class="map-outer">

            <!--Map Canvas-->
			<div class="map-canvas" id="map_canvas" ></div>
            <!--<div class="map-canvas" data-zoom="10" data-lat="-37.817085" data-lng="144.955631" data-type="roadmap" data-hue="#ffc400" data-title="Envato" data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
            </div>-->
			
        </div>
    </section>
    <!--End Map Section-->
    
    <!--Main Footer-->
	   <?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/revolution.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/validate.js"></script>
<script src="./js/script.js"></script>

<!--Google Map APi Key-->
<script type="text/javascript"
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZAsRvE2cZf7qZEsChRXC4y4wZTArlsf0&sensor=false">
</script>

<script type="text/javascript">
    var parliament = new google.maps.LatLng(12.9387298, 80.1387617);
	var marker;
    
  function initialize() {
    var myOptions = {
      zoom: 4,
      center: parliament ,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    
     var marker = new google.maps.Marker({
      position: parliament, 
      map: map, 
      title:"LambdaDigital Private Limited"
  });   
  }

</script>
<!--End Google Map APi-->
	<style type="text/css">
    
    .error{
        color:#d12f19;
        font-size:12px;	
        }
    .success{	
        color:#006600;
        font-size:12px;
                
        }
    </style>




</body>
</html>
