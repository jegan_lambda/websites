<!--Main Footer-->
    <footer class="main-footer">
		<!--Upper Box-->
        
       <!-- <div class="upper-box">
        	<div class="auto-container">
            	<div class="clearfix">
                	<div class="pull-left">
                    	<h2>Have any question or need for any technology or business consultation? </h2>
                    </div>
                    <div class="pull-right">
                    	<a href="./contact.html" class="theme-btn btn-style-two">Contact Us Now!!</a>
					</div>
                </div>
            </div>
        </div>-->
        
        <!--Widgets Section-->
        <div class="widgets-section">
        	<div class="auto-container">
            	<div class="row clearfix">
                	<!--Big Column-->
                	<div class="big-column col-md-12 col-sm-12 col-xs-12">
                    	<div class="row clearfix">

                            <!--Footer Column / Logo Widget-->
                        	<div class="footer-column col-md-4 col-sm-4 col-xs-12">
                            	<div class="footer-widget logo-widget">
                                	<div class="widget-content">
									<h2>LambdaDigital</h2>
                                    	<!--<div class="logo-box">
                                        	<a href="./index.html"><img src="./images/footer-logo.png" alt="" /></a>                                     
										</div>-->
										<div class="text">Simplify your business processes using LambdaDigital's customized and unified solutions. </div>
                                        <div class="newsletter-form">
                                        	<h2>Newsletter</h2>
                                            <form method="post" name="footer_form" action="#">
                                                <div class="form-group">
                                                    <input type="email" id="f_user_mail" name="f_user_mail" value="" placeholder="Enter your email" required="" />
                                                    <button name="button1" type="submit"><span class="icon flaticon-send-message-button"></span></button>
													<?php 
													if (isset($_POST['button1'])) 
													{ 
														$email = $_POST['f_user_mail'];
														$subhead = 'Request for Newsletter';
														$email_from = $email;
														$email_to = 'sales@lambdadigital.co.in';
														//'booking@shrisubhamresidency.com, shrisubhamresidency@gmail.com';//replace with your email
														$body = 'Newsletter request from website : ' . $email;
														$subject = $subhead;
														//$headers .='X-Mailer: PHP/' . phpversion();
														//$headers .= "MIME-Version: 1.0\r\n";
														//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
														$success = mail($email_to, $subject, $body, 'From: <'.$email_from.'>');
														$message = "Newsletter request sent succesfully";
														echo "<script type='text/javascript'>alert('$message');</script>";
													} 
													?>
												</div>
                                            </form>
																									
												
												
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Footer Column / Links Widget-->
                        	<div class="footer-column col-md-4 col-sm-4 col-xs-12">
                            	<div class="footer-widget links-widget">
                                	<h2>Quick Links</h2>
                                    <div class="widget-content">
                                    	<div class="row clearfix">
                                            <ul class="list col-md-6 col-sm-6 col-xs-12">
                                                <li><a href="/">Home</a></li>
                                              <!--  <li><a href="#">Features</a></li>-->
                                                <li><a href="./about.php">About us</a></li>
												<li><a href="./contact.php">Contact us</a></li>
                                                <!--  <li><a href="#">Services</a></li>-->
                                                <!-- <li><a href="#">Projects</a></li> -->
                                                <!-- <li><a href="#">Pricing</a></li> -->
                                            </ul>
                                            <ul class="list col-md-6 col-sm-6 col-xs-12">
												<li><a href="./technology.php">Technology</a></li>
												<li><a href="./leadership.php">Leadership</a></li>
                                                <!-- <li><a href="#">News</a></li>
                                                <li><a href="#">Shop</a></li> --
                                                <li><a href="./contact.php">Contact us</a></li>
                                                 <!--<li><a href="#">FAQ</a></li>-->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							
							<!--Footer Column / Address Widget-->
                        	<div class="footer-column col-md-4 col-sm-4 col-xs-12">
                            	<div class="footer-widget address-widget">
                                	<h2>Our Address</h2>
                                    <div class="widget-content">
										<ul class="list-style-one">
                                        	<li><span class="icon flaticon-map-marker"></span>No.4, 2nd Floor, Kamaraj Colony, Chitlapakkam, Chennai, TN-600064, IND</li>
                                            <li><span class="icon flaticon-telephone-handle-silhouette"></span> +91 9840 358 348</li>
                                            <li><span class="icon fa fa-envelope"></span><a href="mailto:sales@lambdadigital.co.in">sales@lambdadigital.co.in </a> </li>
                                            <li><span  class="icon fa fa-clock-o"></span>Office Hours: Week days 10:00am to 7:00pm </li>
											<!--<span class="icon fa fa-clock-o"></span> 10:00am - 7:00pm <br />Sunday Closed-->

                                        </ul>
                                    </div>
                                </div>
                            </div>
							
							
                        </div>
                    </div>

                    <!--Big Column-->
                	<!--<div class="big-column col-md-6 col-sm-12 col-xs-12">
                    	<div class="row clearfix">

                            <!--Footer Column / News Widget--
                        	<div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="footer-widget news-widget">
                                	<h2>Latest News</h2>
                                    <div class="widget-content">
                                    	<?php
										$rss = new DOMDocument();
										$rss->load('http://feeds.feedburner.com/TechCrunchIT');
										$feed = array();
										foreach ($rss->getElementsByTagName('item') as $node) {
											$item = array ( 
												'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
												'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
												'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
												'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
												'image' => $node->getElementsByTagName('thumbnail')->item(0) ? $node->getElementsByTagName('thumbnail')->item(0)->getAttribute('url') : '',
												);
											array_push($feed, $item);
										}
										$limit = 3;
										for($x=0;$x<$limit;$x++) {
											$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
											$link = $feed[$x]['link'];
											$description = $feed[$x]['desc'];
											$date = date('l F d, Y', strtotime($feed[$x]['date']));
											$image_url = $feed[$x]['image'];
		                                ?> 
                
                                    	<div class="news-post">
                                        	<a href="<?php 
											echo $link 
											?>" 
											target="_blank" title="
											<?php 
											$title 
											?>">
											<?php echo $title ?>
                                            <div class="posted-date">
											<?php echo $date 
											?></div>
                                        </div>
                                        
                                        <?php		
										}	
									    ?>   
     
                                    </div>
                                </div>
                            </div> 
							
                        </div>
                    </div>-->
              </div>
          </div>
      </div>

         <!--Footer Bottom-->
		 <div class="footer-bottom">
			<div class="auto-container">
				<div class="row clearfix">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<div class="copyright"> &copy; 2018 <a href="#">LambdaDigital</a>. All rights reserved.</div>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 text-right">
						<ul class="social-icon-one">
							<li><a href="https://www.facebook.com/Lambdadigital-Pvt-Ltd-549625572062402/" class="fa fa-facebook"></a></li>
							<li><a href="https://twitter.com/LambdaDigital" class="fa fa-twitter"></a></li>
							<!-- <li><a href="#" class="fa fa-google-plus"></a></li>
							<!-- <li><a href="#" class="fa fa-pinterest-p"></a></li>
							<li><a href="#" class="fa fa-vimeo"></a></li> -->
							<li><a href="https://www.linkedin.com/company/lambdadigital/?lipi=urn%3Ali%3Apage%3Ad_flagship3_company_admin%3Bfgr4KtIBR9%2B2migC%2BAF4vg%3D%3D" class="fa fa-linkedin"></a></li>
						</ul>
					</div>
				</div>
			</div>
		 </div>

    </footer>

    
</div>
<!--End pagewrapper-->
