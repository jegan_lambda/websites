    <!-- Main Header-->
     <header class="main-header">
        <!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-outer">
                    	<div class="logo"><a href="index.php"><img src="./images/logo.png" alt="" title="" /></a></div>
                    </div>
                    
                    <div class="pull-right upper-right clearfix">
                    	
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-placeholder"></span></div>
                            <ul>
                            	<li><strong>Office Location</strong></li>
                                <li>No.4, 2nd Floor, Kamaraj Colony, Chitlapakkam, Ch-64</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-empty-email"></span></div>
                            <ul>
                            	<li><strong><a href="mailto:sales@lambdadigital.co.in">sales@lambdadigital.co.in </a></strong></li>
                                <li>Office Hour: Week days 10:00am - 7:00pm</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<a data-toggle="modal" data-target="#myModal" href="#" class="quote-btn theme-btn btn-style-one">Login</a>    
							</div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower">
            
        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
								<?php
	
  
 
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
  $activePage = basename($_SERVER['PHP_SELF'],'.php');
 
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
  $activePage = basename($_SERVER['PHP_SELF'],'.php');
  
 }
 
?>
                                <li  <?php  if ($activePage =="index") {?> class="current" <?php } ?>><a href="index.php"> Home</a></li>
                                <li  <?php if ($activePage =="about") {?> class="current" <?php } ?> ><a href="about.php">About us</a></li>
                                <li  <?php if ($activePage =="technology" || $activePage =="outsourcing" || $activePage =="consulting"||$activePage =="productdevelopment" ) {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Services</a>
                                    <ul>
                                        <!--li><a href="./services.html">Services Layout 01</a></li>
                                        <li><a href="./services-two.html">Services Layout 02</a></li>
                                        <li><a href="./services-single.html">Financial Advise</a></li>
                                        <li><a href="./services-single.html">Google Analyze</a></li-->									
                                        <li><a href="technology.php">Technology</a></li>
                                        <li><a href="./outsourcing.php">Outsourcing</a></li>
                                        <li><a href="./consulting.php">Consulting</a></li>
                                        <li><a href="./productdevelopment.php">Product Development</a></li>
                                       
                                    </ul>
                                </li>
                                <!--<li <?php if ($activePage =="retail" || $activePage =="automotive" || $activePage =="transportation"||$activePage =="bfsi"||$activePage =="non_profit" ) {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Industries</a>

                                    <ul>
                                    	<li><a href="retail.php">Retail</a></li>
                                        <li><a href="automotive.php">Automotive</a></li>
                                        <li><a href="transportation.php">Transportation</a></li>
									    <li><a href="bfsi.php">BFSI</a></li>
										<li><a href="non_profit.php">Non-profit Institutions</a></li>
                                        
                                    </ul>
                                </li>-->
                                <li <?php if ($activePage =="leadership" || $activePage =="partners") {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Company</a>
                                    <ul>
                                        <!--<li><a href="about.php">About us</a></li>-->
                                        <li><a href="leadership.php">Leadership</a></li>
                                        <li><a href="partners.php">Partners</a></li>
                                    </ul>
                                </li>
                                <li <?php if ($activePage =="contact") {?> class="current" <?php } ?> ><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <div class="outer-box">
                    	<!--Search form
                        <div class="search-form">
                            <form method="post" action="contact.php" />
                                <div class="form-group">
                                    <input type="text" name="search" value="" placeholder="Search here.." required="" />
                                    <button type="submit" class="theme-btn"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="./index.php" class="img-responsive"><img src="./images/logo-small.png" alt="" title="" /></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li <?php if ($activePage =="index") {?> class="current" <?php } ?>><a href="./index.php">Home</a></li>
                                <li <?php if ($activePage =="about") {?> class="current" <?php } ?>><a href="about.php">About us</a></li>
                                <li <?php if ($activePage =="technology" || $activePage =="outsourcing" || $activePage =="consulting"||$activePage =="productdevelopment" ) {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Services</a>
                                    <ul>
                                        <!--li><a href="./services.html">Services Layout 01</a></li>
                                        <li><a href="./services-two.html">Services Layout 02</a></li>
                                        <li><a href="./services-single.html">Financial Advise</a></li>
                                        <li><a href="./services-single.html">Google Analyze</a></li-->
                                        <li ><a href="technology.php">Technology</a></li>
                                        <li><a href="./outsourcing.php">Outsourcing</a></li>
                                        <li><a href="./consulting.php">Consulting</a></li>
                                        <li><a href="./productdevelopment.php">Product Development</a></li>
                                    </ul>
                                </li>
                                <!--<li <?php if ($activePage =="retail" || $activePage =="automotive" || $activePage =="transportation"||$activePage =="bfsi"||$activePage =="non_profit" ) {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Industries</a>
                                    <ul>
                                    	<li><a href="retail.php">Retail</a></li>
                                        <li><a href="automotive.php">Automotive</a></li>
                                        <li><a href="transportation.php">Transportation</a></li>
									    <li><a href="bfsi.php">BFSI</a></li>
										<li><a href="non_profit.php">Non-profit Institutions</a></li>
                                    </ul>
                                </li>-->
                                <li <?php if ($activePage =="leadership" || $activePage =="partners") {?> class="dropdown current" <?php } else { ?> class="dropdown" <?php } ?>><a href="#">Company</a>
                                    <ul>
                                        <!--<li><a href="about.php">About us</a></li>-->
                                        <li><a href="leadership.php">Leadership</a></li>
                                        <li><a href="partners.php">Partners</a></li>
                                    </ul>
                                </li>
                                <li <?php if ($activePage =="contact") {?> class="current" <?php } ?> ><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
		
	
    </header>
    <!--End Main Header -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header main-header header-lower">
			<!--<button type="button" class="close" data-dismiss="modal" action="#">X</button>-->
			  <h4 class="modal-title">Login</h4>
			</div>
			<div class="modal-body">
				<!-- Consulting Form -->
                <div class="consulting-form">
					<!--Contact Form-->
					<form>
						<div class="form-group">
							<div class="field-label">Username<span class="required-tag">*</span></div>
							<input type="text" name="username" placeholder="Enter your username" required="" />
						</div>
						<div class="form-group">
							<div class="field-label">Password<span class="required-tag">*</span></div>
							<input type="password" name="password" value="" placeholder="Enter your password" required="" />
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-default" >Login</button>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
			  <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		  </div>
		</div>
	  </div>
  