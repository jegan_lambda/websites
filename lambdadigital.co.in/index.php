<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital Private Limited</title>

<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />

<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/revolution-slider.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
		<?php include_once('header.php');    ?>
    <!--End Main Header -->
    
	<!--Main Slider-->
    <section class="main-slider" data-start-height="800" data-slide-overlay="yes">
    	
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                	
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/image-1.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="./images/main-slider/image-1.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 
                    
                    <div class="overlay-layer"></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="-80" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2>Welcome to LambdaDigital <br /> IT Solutions for  your business needs</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="40" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text">We provide all kind of technology, outsourcing & consulting services <br />for over 4 years with pride and love!!</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="130" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="./technology.php" class="theme-btn btn-style-two">See all services</a> &nbsp; &nbsp; <a href="./contact.php" class="theme-btn btn-style-three">Free Consultation</a></div>
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/image-3.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="./images/main-slider/image-3.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 
                    
                    <div class="overlay-layer"></div>
                    
                    <div class="tp-caption sft sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="-80" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2 class="text-center">We provide best <br /> IT Offshore Services</h2></div>
                    
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="40" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text text-center">"League of extraordinary individuals" with sophisticated <br />technology skills & domain background</div></div>
                    
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="130" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="./technology.php" class="theme-btn btn-style-two">See all services</a> &nbsp; &nbsp; <a href="./contact.php" class="theme-btn btn-style-three">Free Consultation</a></div>
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/image-2.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="./images/main-slider/image-2.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" /> 
                    
                    <div class="overlay-layer"></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme" data-x="right" data-hoffset="-15" data-y="center" data-voffset="-80" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2 class="text-right">A Technology<br /> Outsourcing & Consulting Company</h2></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme" data-x="right" data-hoffset="-15" data-y="center" data-voffset="40" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text text-right">Choice of Engagement, Delivery and Costing models<br /> </div></div>
                    
                    <div class="tp-caption sfr sfb tp-resizeme" data-x="right" data-hoffset="-15" data-y="center" data-voffset="130" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="./technology.php" class="theme-btn btn-style-two">See all services</a> &nbsp; &nbsp; <a href="./contact.php" class="theme-btn btn-style-three">Free Consultation</a></div>
                    
                    </li>
                    
                </ul>
                
            	<div class="tp-bannertimer"></div>
            </div>
        </div>
    </section>
  	<!--End Main Slider-->
    
    <!--Welcome Section-->
    <section class="welcome-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<h2>Welcome to Lambda<span class="theme_color">Digital</span></h2>
                <div class="text">Empowering business through technology innovation.</div>
                <div class="separater"></div>
            </div>
            <!--End Sec Title-->
            
            <div class="row clearfix">
            	<!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./technology.php"><img src="./images/resource/welcome-1.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./technology.php">Business Advisor</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./technology.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./outsourcing.php"><img src="./images/resource/welcome-2.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./outsourcing.php">Market Analyize</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./outsourcing.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!--Services Block One-->
            	<div class="services-block-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<a href="./consulting.php"><img src="./images/resource/welcome-3.jpg" alt="" /></a>
                        </div>
                        <div class="lower-box">
                        	<h3><a href="./consulting.php">Consultation</a></h3>
                            <div class="text">Get financial advise wih expert</div>
                            <!--Arrow Box-->
                            <div class="arrow-box">
                            	<a href="./consulting.php" class="icon flaticon-right-arrow-1"></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            
        </div>
    </section>
    <!--End Welcome Section-->
    
    <!--Services Section-->
    <section class="services-section">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                	<!--Sec Title-->
                    <div class="sec-title">
                    	<h2>Our Services</h2>
                        <div class="text">IT Solutions for all your needs. Technology, outsourcing & consulting services.</div>
                        <div class="separater"></div>
                    </div>
                </div>
                <div class="pull-right">
                	<a href="./technology.php" class="theme-btn services-btn btn-style-four">See all services</a>
                </div>
            </div>
            <div class="row clearfix">
            	<!--Services Block Two-->
            	<div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-calculator-1"></span>
                        </div>
                        <h3><a href="./technology.php">Technology</a></h3>
                        <div class="text">Define a IT roadmap that keeps pace with an ever-changing digital business landscape using our technology services.
						</div>
                    </div>
                </div>
                
                <!--Services Block Two-->
            	<div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-travel"></span>
                        </div>
                        <h3><a href="./outsourcing.php">Outsourcing</a></h3>
                        <div class="text">Outsource your IT solutions to LambdaDigital and focus on running your core Business, your time is limited & precious.</div>
                    </div>
                </div>
                
                <!--Services Block Two-->
            	<div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-arrows-9"></span>
                        </div>
                        <h3><a href="./consulting.php">Consulting</a></h3>
                        <div class="text">We focus on delivering holistic solutions right from strategic IT consulting, to design, development and support of custom solutions.
						</div>
                    </div>
                </div>
                
                <!--Services Block Two-->
            	<div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-bar-chart"></span>
                        </div>
                        <h3><a href="./productdevelopment.php">Product Development</a></h3>
                        <div class="text">Full product development life cycle support - from ideation, design, UX arch, development to testing and maintenance.</div>
                    </div>
                </div>
                
                <!--Services Block Two-->
            	<div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-user-3"></span>
                        </div>
                        <h3><a href="./consulting.php">Business Consulting</a></h3>
                        <div class="text">If you are a small or medium sized enterprise, you can benefit by engaging us in business process re-engineering, automation etc.</div>
                    </div>
                </div>
                
                <!--Services Block Two-->
            	<!--div class="services-block-two col-lg-4 col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<span class="icon flaticon-travel-1"></span>
                        </div>
                        <h3><a href="./services-single.html">Business Consulting</a></h3>
                        <div class="text">Vivamus aliquet rutrusm duia variu sath Mauris ornoare tortor. Dosi tm lorem ipsum ample text.</div>
                    </div>
                </div-->
                
            </div>
        </div>
    </section>
    <!--End Services Section-->
    
    <!--Deafult Section-->
    <section class="default-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Column-->
                <div class="about-column column col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                        <div class="sec-title">
                            <h2>About Us</h2>
                            <div class="separater"></div>
                        </div>
                        <div class="text">
                        	<p>A management team with extensive experience in building high-performance delivery teams.<br>
                            Best practices from experience in setting up & running offshore centres & Captives for notable Fortune 500 companies.<br>
							Choice of Engagement, Delivery and Costing models.<br>
							Introducing “state of the art” technology components, proven OSS frameworks/tools, replacing outdated and unsupported technologies
							</p>
                        </div>
                        <div class="clearfix">
                        	<div class="author-info pull-left">
                            	<div class="img-thumb">
                                	<img src="./images/resource/author-1.jpg" alt="" />                                
								</div>
                                <h4>Saravanan Ananthakrishnan</h4>
                                <div class="designation">CEO & Founder of LambdaDigital</div>
                            </div>
                            <div class="pull-right signature">
                            	<div class="signature-inner">
                                	<img src="./images/resource/signature.png" alt="" />
                                </div>
                            </div>
                        </div>
                        <!--<a href="#" class="theme-btn btn-style-five learn-more">Learn More</a>-->
                    </div>
                </div>
                <!--Column-->
                <div class="column col-md-6 col-sm-12 col-xs-12">
				
                	<!--<div class="inner">
                        div class="sec-title">
                            <h2>Frequently Asked Question</h2>
                            div class="separater"></div>
                        </div>
                        
                        <!--Accordion Box
                        <ul class="accordion-box">-->
                        
                            <!--Block-->
                            <!--<li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How can i get help from LambdaDigital?</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <p>Lorem ipsum dolor sit amet, vix an natum labitur eleifnd, mel am laoreet menandri. Ei justo complectitur duo. Ei mundi solet ut soletu possit quo. Sea cu justo laudem.</p>
                                    </div>
                                </div>
                            </li>-->
    
                            <!--Block-->
                           <!-- <li class="accordion block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>What about loan programs & Advantage?</div>
                                <div class="acc-content current">
                                    <div class="content">
                                        <p>Lorem ipsum dolor sit amet, vix an natum labitur eleifnd, mel am laoreet menandri. Ei justo complectitur duo. Ei mundi solet ut soletu possit quo. Sea cu justo laudem.</p>
                                    </div>
                                </div>
                            </li>
    
                            <!--Block-->
                            <!--<li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>What abou invest plan & process?</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <p>Lorem ipsum dolor sit amet, vix an natum labitur eleifnd, mel am laoreet menandri. Ei justo complectitur duo. Ei mundi solet ut soletu possit quo. Sea cu justo laudem.</p>
                                    </div>
                                </div>
                            </li>
    
                        </ul>
                        
                    </div>
                </div>
            </div>
            
            <!--Fun Facts Section-->
            <div class="fact-counter-column">
                <div class="fact-counter">
                    <div class="auto-container">
                        <div class="row clearfix">
                        
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="3500" data-stop="5">5</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Completed Projects</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="1">1</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Years of Experience</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="10">10</span><span class="plus-icon">+</span>
                                    </div>
                                    <h4 class="counter-title">Expert Workers</h4>
                                </div>
                            </div>
                    
                            <!--Column-->
                            <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                <div class="inner">
                                    <div class="count-outer count-box">
                                        <span class="count-text" data-speed="2000" data-stop="2">2</span>
                                    </div>
                                    <h4 class="counter-title">Clients<br/></h4>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!--End Default Section-->
    
    <!--fluid Section-->
    <!--<section class="fluid-section">
    	<!--BG Image--
    	<div class="background-image" style="background-image:url(./images/resource/fullwidth-1.jpg);"></div>
        
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="testimonial-column col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                    	<!--Testimonial Style--
                        <div class="testimonial-inner">
                        	<div class="quote-icon"><span class="icon flaticon-left-quote"></span></div>
                            <div class="testimonial-style-one owl-carousel owl-theme">
                                <div class="testimonial-box">
                                    <div class="inner-box">
                                        <div class="text">Looking cautiously round, ascerta that they were not overheard, the tw hagso cowered nearer to the fire acer.</div>
                                        <h4>Saravanan A K</h4>
                                    </div>
                                </div>
                                <div class="testimonial-box">
                                    <div class="inner-box">
                                        <div class="text">Looking cautiously round, ascerta that they were not overheard, the tw hagso cowered nearer to the fire acer.</div>
                                        <h4>Saravanan A K</h4>
                                    </div>
                                </div>
                                <div class="testimonial-box">
                                    <div class="inner-box">
                                        <div class="text">Looking cautiously round, ascerta that they were not overheard, the tw hagso cowered nearer to the fire acer.</div>
                                        <h4>Saravanan A K</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Content Column--
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                	<div class="inner">
                    	<h2>Why Choose Us</h2>
                        <div class="featured-info-block">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-blank-paper"></span>                                </div>
                                <h3><a href="./services-single.html">Trusted Vendor</a></h3>
                                <div class="text">Looking cautiously round, to ascertain that they were not overheard, the two hags cowered.</div>
                            </div>
                        </div>
                        <div class="featured-info-block">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-user-2"></span>                                </div>
                                <h3><a href="./services-single.html">Professional Advisor</a></h3>
                                <div class="text">Looking cautiously round, to ascertain that they were not overheard, the two hags cowered.</div>
                            </div>
                        </div>
                        <div class="featured-info-block">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-trophy"></span>                                </div>
                                <h3><a href="./services-single.html">Award’s Winner</a></h3>
                                <div class="text">Looking cautiously round, to ascertain that they were not overheard, the two hags cowered.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--End fluid Section-->
    
    <!--Project Section-->
  <!--  <section class="project-section no-padding-bottom">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                    <!--Sec Title-->
                  <!--  <div class="sec-title">
                        <h2>Recent Projects</h2>
                        <div class="separater"></div>
                    </div>
                </div>
                <div class="pull-right">
                	<a href="./project.html" class="theme-btn btn-style-six">See All Projects</a>
                </div>
            </div>
        </div>-->
       <!-- <div class="four-item-carousel owl-carousel owl-theme">-->
        	<!--Default Gallery Item-->
        <!--	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/1.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                      <!--  <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
        	<!--<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/2.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                      <!--  <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
        	<!--<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/3.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                       <!-- <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
        <!--	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/4.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                     <!--   <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
        <!--	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/1.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                      <!--  <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
        <!--	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/2.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                       <!-- <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
    <!--    	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/3.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                     <!--   <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Default Gallery Item-->
 <!--       	<div class="default-gallery-item">
            	<div class="inner-box">
                    <figure class="image-box"><img src="./images/gallery/4.jpg" alt="" /></figure>
                    <!--Overlay Box-->
                       <!-- <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                            	<div class="border-box"></div>
                                <h4><a href="./project-single.html">Google Analyzing</a></h4>
                                <a href="./project-single.html" class="option-btn"><span class="flaticon-right-arrow"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <!--End Project Section-->
    
    <!--Price Box-->
    <!--section class="price-section">
    	<div class="auto-container"-->
        	<!--Sec Title-->
            <!--div class="sec-title centered">
            	<div class="sub-title">Pricing Plan</div>
                <h2>Choose your plan. No hiddden charge!</h2>
                <div class="separater"></div>
            </div>
            <div class="clearfix"-->
            	<!--Price Box-->
            	<!--div class="price-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box">
                        	<div class="price-title">Starter</div>
                            <div class="total-price"><sup>$</sup>28</div>
                        </div>
                        <div class="lower-box">
                        	<ul>
                            	<li>50GB Bandwidth</li>
                                <li>Business & Financ Analysing</li>
                                <li>24 hour support</li>
                                <li>Customer Managemet</li>
                                <li>2 Emails Acounts</li>
                            </ul>
                            <a href="#" class="theme-btn btn-style-seven">Purchase Now</a>
                        </div>
                    </div>
                </div-->
                <!--Price Box-->
            	<!--div class="price-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box">
                        	<div class="price-title">agency</div>
                            <div class="total-price"><sup>$</sup>39</div>
                        </div>
                        <div class="lower-box">
                        	<ul>
                            	<li>50GB Bandwidth</li>
                                <li>Business & Financ Analysing</li>
                                <li>24 hour support</li>
                                <li>Customer Managemet</li>
                                <li>2 Emails Acounts</li>
                            </ul>
                            <a href="#" class="theme-btn btn-style-seven">Purchase Now</a>
                        </div>
                    </div>
                </div-->
                <!--Price Box-->
            	<!--div class="price-box col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box">
                        	<div class="price-title">Corpoate</div>
                            <div class="total-price"><sup>$</sup>45</div>
                        </div>
                        <div class="lower-box">
                        	<ul>
                            	<li>50GB Bandwidth</li>
                                <li>Business & Financ Analysing</li>
                                <li>24 hour support</li>
                                <li>Customer Managemet</li>
                                <li>2 Emails Acounts</li>
                            </ul>
                            <a href="#" class="theme-btn btn-style-seven">Purchase Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section-->
    <!--End Price Box-->
    
    <!--Client Section-->
  <!--  <section class="client-section">
    	<div class="auto-container">-->
        	<!--Sec Title-->
   <!--         <div class="sec-title centered">
            	<div class="sub-title">Testimonials</div>
                <h2>What Clients Say</h2>
                <div class="separater"></div>
            </div>
            <div class="two-item-carousel owl-carousel owl-theme">-->
            	<!--Client Box-->
          <!--  	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box">
                            	<span class="icon fa fa-facebook"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Jannatul Fa.</h3>
                        <div class="designation">Photographer LambdaDigital</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>-->
                
                <!--Client Box-->
         <!--   	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box instagram">
                            	<span class="icon fa fa-instagram"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Rashed Kabir</h3>
                        <div class="designation">UI/UX Designer of CG</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>-->
                
                <!--Client Box-->
         <!--   	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box">
                            	<span class="icon fa fa-facebook"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Jannatul Fa.</h3>
                        <div class="designation">Photographer LambdaDigital</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>-->
                
                <!--Client Box-->
            <!--	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<div class="social-box instagram">
                            	<span class="icon fa fa-instagram"></span>
                            </div>
                        	<img src="./images/resource/client-1.jpg" alt="" />
                        </div>
                        <h3>Rashed Kabir</h3>
                        <div class="designation">UI/UX Designer of CG</div>
                        <div class="rating">
                        	<span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-full"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div class="text">Looking cautiously round, ascertan that they were not overheard,cowered nearer the fire chuckd utinam that consulatu eos, est facilis suscipiantur</div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>-->
    <!--End Client Section-->
    
    <!--Call To Action-->
<!--    <section class="call-to-action">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<div class="column col-md-8 col-sm-12 col-xs-12">
                	<div class="text">We Are The Best Consulting Company Ever!!</div>
                </div>
                <div class="column text-right col-md-4 col-sm-12 col-xs-12">
                	<a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                </div>
            </div>
        </div>
    </section>-->
    <!--End Call To Action-->
    
    <!--News Section-->
    <!-- <section class="news-section">
    	<div class="auto-container">
        	<div class="sec-title">
            	<h2>Latest News</h2>
                <div class="separater"></div>
            </div>
            <div class="row clearfix">
                <!--Left Column --
                <div class="left-column col-md-8 col-sm-12 col-xs-12">
                    <div class="two-item-carousel owl-carousel owl-theme">
                    	<!--News Style One--
							<?php
										$rss = new DOMDocument();
										$rss->load('http://feeds.feedburner.com/TechCrunchIT');
										$feed = array();
										foreach ($rss->getElementsByTagName('item') as $node) {
											$item = array ( 
												'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
												'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
												'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
												'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
												'image' => $node->getElementsByTagName('thumbnail')->item(0) ? $node->getElementsByTagName('thumbnail')->item(0)->getAttribute('url') : '',
												);
											array_push($feed, $item);
										}
										$limit = 5;
										for($x=0;$x<$limit;$x++) {
											$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
											$link = $feed[$x]['link'];
											$description = $feed[$x]['desc'];
											$date = date('l F d, Y', strtotime($feed[$x]['date']));
											$image_url = $feed[$x]['image'];
											//echo '<div style="width:300px;" height="300px">';
											//	echo '<p style="font-size:"19px"><strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
											//echo '<small><em>Posted on '.$date.'</em></small></p>';
											
											//echo '<div>';
											//echo '<p><img src="' . $image_url . '" alt="Screen shot" height="200" width="200" /></p>';
											//echo '<figure class="post-thumb"><a href="' . $link . '"><img src="' . $image_url . '" alt="" /></a></figure>';
											
		                         ?> 
                     


					 
					

					   <div class="news-style-one">
                            <div class="inner">
                                <div class="image-box">
                                    <div class="image">
                                        <a href="<?php echo $link ?>" target="_blank" title="<?php $title ?>">
										<img src="<?php echo $image_url ?>" alt="" /></a>                                    </div>
                                </div>
                                <div class="lower-box">
                                    <div class="date"><?php echo $date ?> </div>
                                    <h3> <a href="<?php echo $link ?>" target="_blank" title="<?php $title ?>">
									<?php echo $title ?></a></h3>
                                </div>
                            </div>
                        </div>
                     
					 
   									 <?php		
										}	
									  ?>   
       
					 
					 
					 
                    </div>
                </div>
				
				
				
                <!--Right Column--
                <div class="right-column col-md-4 col-sm-12 col-xs-12">
					
					<?php
										$rss = new DOMDocument();
										$rss->load('http://feeds.feedburner.com/TechCrunch/startups');
										$feed = array();
										foreach ($rss->getElementsByTagName('item') as $node) {
											$item = array ( 
												'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
												'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
												'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
												'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
												'image' => $node->getElementsByTagName('thumbnail')->item(0) ? $node->getElementsByTagName('thumbnail')->item(0)->getAttribute('url') : '',
												);
											array_push($feed, $item);
										}
										$limit = 4;
										for($x=0;$x<$limit;$x++) {
											$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
											$link = $feed[$x]['link'];
											$description = $feed[$x]['desc'];
											$date = date('l F d, Y', strtotime($feed[$x]['date']));
											$image_url = $feed[$x]['image'];
											//echo '<div style="width:300px;" height="300px">';
											//	echo '<p style="font-size:"19px"><strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
											//echo '<small><em>Posted on '.$date.'</em></small></p>';
											
											//echo '<div>';
											//echo '<p><img src="' . $image_url . '" alt="Screen shot" height="200" width="200" /></p>';
											//echo '<figure class="post-thumb"><a href="' . $link . '"><img src="' . $image_url . '" alt="" /></a></figure>';
											
		                         ?> 
				
				
                    <!--News Style Two--
                    <div class="news-style-two">
                    	<div class="inner-box">
                        	<div class="image">
							
							<a href="<?php echo $link ?>" target="_blank"><img  src="<?php echo $image_url ?>"></a>
							</div>
                            <h3><a href="<?php echo $link ?>" target="_blank" title="<?php $title ?>"><?php echo $title ?></a></h3>
                            <div class="date"><?php echo $date ?></div>
                        </div>
                    </div>
                
				
                         
   									 <?php		
										}	
									  ?>   
       
	   
	   
                    </div>
					

                </div>
            </div>
        </div>
    </section> -->

    <!--End News Section-->
    
    <!--Consultation Section-->
    <section class="consultation-section" style="background-image:url(./images/background/2.jpg);">
    	<div class="auto-container">
        	<!--Content Box-->
            <div class="content-box clearfix">
            	<!--Content Column-->
            	<div class="content-column">
                	<div class="inner">
                    	<div class="sub-title">Request</div>
                        <h3>Free Consultation</h3>
                        <div class="text">Any kind of business or technology consulting don’t hesitate to contact us for immediate customer support.</div>
                        <div class="call"><strong>Call us</strong> for immediate support to this number <span class="number">+91 9840 358 348</span></div>
                    </div>
                </div>
                <!--Form Column-->
                <div class="form-column">
                	
                    <!-- Consulting Form -->
                    <div class="consulting-form">
                            
                        <!--Contact Form-->
                        <form method="post" action="index.php">
                            <div class="form-group">
                                <div class="field-label">Your Name <span class="required-tag">*</span></div>
                                <input type="text" name="username" placeholder="Enter your full name" required="" />
                            </div>
                               
                            <!--<div class="form-group">
                                <div class="field-label">Your Phone Number</div>
                                <input type="text" name="phone" placeholder="Phone *" required="" />
                            </div>-->
                            
							<div class="form-group">
								<div class="field-label">Your Email</div>
                                <input type="email" name="email" id="email" value="" placeholder="Email *" required="" />
                            </div>
							
                            <div class="form-group">
                                <div class="field-label">I would like to discuss <span class="required-tag">*</span></div>
                                <textarea name="user_message"   id="user_message" placeholder="Message *" required="" ></textarea>
                            </div>
                                
                            <div class="form-group">
                                <button class="theme-btn btn-style-five" type="submit" name="submit-form">Get Free Consultation <span class="arow fa fa-long-arrow-right"></span></button>
								<!--<span class="arow fa fa-long-arrow-right"></span></button>
								 &nbsp; &nbsp; &nbsp;<span class="error" style="display:none"><h1> Please Enter Valid Data</h1></span>
                                    <span  class="success" style="display:none"> <h1>Thank you ...</h1> </span>-->
                            </div>
							<?php 
								if(isset($_REQUEST['email'])){
									$name = $_POST['username'];
									$email = $_POST['email'];
									//$phone = $_POST['phone'];
									$subhead = 'Request Free Consultation from website';
									$comments = $_POST['user_message'];
									$email_from = $email;
									$email_to = 'sales@lambdadigital.co.in';
									//'booking@shrisubhamresidency.com, shrisubhamresidency@gmail.com';//replace with your email
									$body = 'Name: ' . $name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Special Request: ' . $comments;
									$subject = $subhead;
									//$headers .='X-Mailer: PHP/' . phpversion();
									//$headers .= "MIME-Version: 1.0\r\n";
									//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
									$success = mail($email_to, $subject, $body, 'From: <'.$email_from.'>');
									$message = "Free consultation request sent succesfully";
									echo "<script type='text/javascript'>alert('$message');</script>";
								}
							?>
                        </form>
                    </div>
                    <!--End Comment Form --> 
                </div>
            </div>
        </div>
    </section>
    <!--Consultation Section-->
    
    <!--Sponsors Section-->
    <section class="sponsors-section">
    	<div class="auto-container">
        	<div class="carousel-outer">
                <!--Sponsors Slider-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_1.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_2.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_3.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_4.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_5.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_6.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_7.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_8.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_9.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_10.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_1.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_2.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_3.png" alt="" /></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="./images/clients/ml_4.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_5.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_6.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_7.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_8.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_9.png" alt="" /></a></div></li>
					<li><div class="image-box"><a href="#"><img src="./images/clients/ml_10.png" alt="" /></a></div></li>
				</ul>
            </div>
    	</div>
    </section>
    <!--End Sponsors Section-->
    
 
    <?php   include_once "footer.php";    ?>

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/revolution.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  $("a[href='#top']").click(function() {
     $("html, body").animate({ scrollTop: 4940 }, "slow");
     return false;
  });
  });
  
  
$(function() {

$(".submit").click(function() {
    var name = $("#username").val();
	var phoneno = $("#phone").val();
	var email = $("#email").val();
	

    var dataString = 'name='+ name + '&phoneno=' + phoneno + '&email=' + email ;
   
	if(name=='' || phoneno=='' || email=='' )
	{
	$('.success').fadeOut(200).hide();

    $('.error').fadeOut(200).show();
	
	}
	
	
	else
	{
	$.ajax({
	type: "POST",
    url: "consultationdb.php",
    data: dataString,
    success: function(data){
		
	
	$('.success').fadeIn(200).show();
    $('.error').fadeOut(200).hide();
	//$('.consulting-form').reset();
	 //$(this).closest('form').find("input[type=text], textarea").val("");
	
		
   }
});




	}
		

    return false;
	});



});
</script>

<style type="text/css">
    
.error{
    color:#d12f19;
    font-size:12px;	
      }
.success{	
    color:#006600;
    font-size:12px;
      }
</style>
</body>
</html>
