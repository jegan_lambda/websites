<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Leadership</title>
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
	<?php   include_once "header.php";    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Leadership</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
					 <li>Company</li>
                    <li>Leadership</li>
                </ul>
            </div>
            <div class="pull-right">
             <!--   <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
	<!--Testimonials-->
	<section class="client-section">
    	<div class="auto-container">

            	<!--Client Box-->
            	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<!--<div class="social-box">
                            	<a href="https://www.linkedin.com/in/saravanan-ananthakrishnan-a2692625/"><span class="icon fa fa-linkedin"></span></a>
                            </div>-->
							<a target="_blank" href="https://www.linkedin.com/in/saravanan-ananthakrishnan-a2692625/">
								<img width="65" height="65" src="./images/clients/user.png" />
							</a>
						</div>
                        <h3>Saravanan Ananthakrishnan</h3>
							<div class="text"><p>Saravanan leads the Technology Innovation group that is responsible for piloting the use and adoption of the latest technologies in Social, Mobility, Predictive Analytics, Cloud Infrastructures, Process Management & Integration.<p/>
							<ul class="list-style-two">
								<li><p>With over 19 years of experience in IT his work involves IT Strategy Consulting, Research & Development, Architecture & Design, Infrastructure Planning, Product Engineering, Delivery Oversight and Resource Management.<p/>
								</li>
								<li><p>Areas of expertise - ML/Predictive Analytics, API Design & Development, Big Data Infrastructures, BPM/EAI/ETL, Data Visualization, GIS, Enterprise Applications, Web Application Development, Social Media Monitoring, Security & Infrastructure architecture, Cloud Infra Management etc.<p/></li>
							</ul></div>
                    </div>
                </div><br/>
				
				<!--Client Box-->
				<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<!--<div class="social-box">
                            	<a href="#"><span class="icon fa fa-linkedin"></span></a>
                            </div>-->
							<a href="#"><img width="65" height="65" src="./images/clients/user.png" /></a>
						</div>
                        <h3>Mathew Thomas</h3>
						<div class="text"><p>Mathew A Thomas is a practicing Charted Accountant with a Ph.D in Information Security from IIT - Madras. He has over 16 years experience in Information Security and nearly six years in Data Analytics.<p/>
						<p>His recent works in Data Science includes:<p/>
						<ul class="list-style-two">
							<li>
							Use of analytics for anomaly or fraud detection and forensics. Developed a model for a State Government to detect tax evasion using Benford's law
							</li>
							<li>
							Currently involved in a speech analytics, identifying key metrics impacting renewals of motor insurance premiums for an Insurance call center operation.
							</li>
							<li>
							Determining locations for a container freight station, based on trade, shipping and demographic forecasts.
							</li>
							<li>
							Market feasibility of Glucose Meters for hospital environments. Study involved focus groups of doctors and nurses and field survey/observations at hospitals
							</li>
							<li>
							Identifying opportunities for application of forecasts, statistical models, segmentation schemes, predictive models and machine learning
							</li>
						</ul>									
						</div>
                    </div>
                </div><br/>
				
			<!--Client Box-->
            	<div class="client-box">
                	<div class="inner-box">
                    	<div class="icon-box">
                        	<!--<div class="social-box">
                            	<a href="https://www.linkedin.com/in/vaidyanathan74/"><span class="icon fa fa-linkedin"></span></a>
                            </div>-->
							<a target="_blank" href="https://www.linkedin.com/in/vaidyanathan74/">
								<img width="65" height="65" src="./images/clients/user.png" />
							</a>
						</div>
                        <h3>Vaidyanathan Swaminathan</h3>
						<div class="text"><p>Vaidyanathan leads the Technology Innovation group that is responsible for piloting the use and adoption of the latest technologies in Social, Mobility, Cloud Infrastructures, Process Management & Integration.<p/>
						<ul class="list-style-two">
							<li><p>With over 19 years of experience in IT his work involves IT Strategy Consulting, Research & Development, Architecture & Design, Infrastructure Planning, Product Engineering, Delivery Oversight and Resource Management.<p/>
							</li>
							<li><p>Areas of expertise - API Design & Development, Data Visualization, GIS, Enterprise Applications, Web Application Development, Social Media Monitoring etc.<p/>
							</li>
						</ul>
						</div>
                    </div>
                </div>

        </div>
    </section>

	
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	 
                <!--Sidebar Side--
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Search --
                        <div class="sidebar-widget search-box">
                        	<form method="post" action="contact.html" />
                               <!-- <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search Here.." />
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>--
                            </form>
						</div>
                                
                        <!--Blog Category Widget--
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title">
                                <h2>Categories</h2>
                            </div>
                            <ul class="blog-cat">
                                <li><a href="#">Budget</a></li>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Invesment</a></li>
                                <li><a href="#">Stock</a></li>
                                <li><a href="#">Financial</a></li>
                            </ul>
                        </div>
                                
                        
                        <!-- Popular Posts --
                       <?php   include_once "sidebar_technews.php";    ?>
                        
                               
                        <!-- Popular Tags --
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h2>Keyword</h2></div>
                            <a href="#">Advise</a>
                            <a href="#">Business</a>
                            <a href="#">Marketing</a>
                            <a href="#">Financial</a>
                            <a href="#">planning</a>
                            <a href="#">consulting</a>
                        </div>
                                                
                    </aside>
                </div>-->
                
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
	<?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
