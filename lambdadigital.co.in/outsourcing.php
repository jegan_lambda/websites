<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Outsourcing</title>
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
   <?php   include_once "header.php"; ?>
    <!--End Main Header -->
    
	<!--Page Title-->
	<div id="pg_tl_App_develop"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Enterprise Application Development</h1>
			</div>
		</section>
	</div>
	
	<div style="display:none;" id="pg_tl_Infra_mgmt"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Infrastructure Management Services</h1>
			</div>
		</section>
	</div>
	
	<div style="display:none;" id="pg_tl_dev_ops"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Development & Operations Services</h1>
			</div>
		</section>
	</div>
	
	<div style="display:none;" id="pg_tl_test_engg"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Test Engineering</h1>
			</div>
		</section>
	</div>

	<div style="display:none;" id="pg_tl_market_research"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Market Research & Analytics</h1>
			</div>
		</section>
	</div>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
					<li>Services</li>
                    <li>Outsourcing</li>
                </ul>
            </div>
            <div class="pull-right">
             <!--   <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Sidebar Category -->
                        <div class="sidebar-widget sidebar-category">
                            <ul class="list">
                                <li class="current" id="left_menu_adms"><a href="javascript:show('App_develop'); javascript:display('pg_tl_App_develop','left_menu_adms');">ADMS</a></li>
                                <li id="left_menu_infra_manag"><a href="javascript:show('Infra_mgmt'); javascript:display('pg_tl_Infra_mgmt','left_menu_infra_manag');">Infra. Management</a></li>
                                <li id="left_menu_devop_eng"><a href="javascript:show('dev_ops'); javascript:display('pg_tl_dev_ops','left_menu_devop_eng');">DevOps Engineering</a></li>
                                <li id="left_menu_test_eng"><a href="javascript:show('test_engg'); javascript:display('pg_tl_test_engg','left_menu_test_eng');">Test Engineering</a></li>
                                <li id="left_menu_pg_tl"><a href="javascript:show('market_research'); javascript:display('pg_tl_market_research','left_menu_pg_tl');">Market Research</a></li>
                            </ul>
                        </div>
                        
                        <!-- Sidebar Testimonial -->
                        <!--<div class="sidebar-widget sidebar-testimonial">
                        	<div class="single-item-carousel owl-carousel owl-theme">
                            	<!--Testimonial Slide-->
                            	<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>-->
                                
                                <!--Testimonial Slide-->
                            	<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                                <!--Testimonial Slide-->
							<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>-->
                        
                        <!-- Sidebar Question Box -->
                        <div class="sidebar-widget sidebar-question">
                        	<div class="info-widget">
                                <div class="inner">
                                    <h3>Have Any Question <br /> Call Us Now</h3>
                                    <h2>+91 9840 358 348</h2>
                                    <a href="#" class="more-detail">More Details</a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Sidebar brochure-->
                        <div class="sidebar-widget sidebar-brochure">
                          <!--	<a class="brochure" href="#"><span class="icon flaticon-pdf"></span> Details Brochure.pdf <span class="download-icon flaticon-download-arrow-1"></span></a>-->
                        </div>
                        
                    </aside>
                </div>
                
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<!--Services Single-->
                	<div class="services-single">
						<div class="services-single-images">
                        	<div class="row clearfix">
                            	<div class="column col-md-8 col-sm-8 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-1.jpg" title="Image Caption Here"><img src="./images/resource/services-single-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-2.jpg" title="Image Caption Here"><img src="./images/resource/services-single-2.jpg" alt="" /></a>
                                    </div>
                                    <div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-3.jpg" title="Image Caption Here"><img src="./images/resource/services-single-3.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
             <!--App Development Block Start-->          
                                          
         <div  id="App_develop"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Application Development & Management Services</h3>
                            <div class="sub-title">Empower your business using LambdaDigital's <span class="theme_color">ADMS practice</span></div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
								<p>LambdaDigital is an Enterprise Application development & IT outsourcing company providing custom web, mobility and enterprise solutions to customers, across the globe. We specialize on OSS and cover application design, development, integration, testing, maintenance and support services. </p>
                                
								<p>We have the ability to work on multiple opensource platforms and domains. We offer an onsite, offshore and offsite model. We combine deep technology expertise on Java, Python, Scala & node.js platforms, and cover mobile / web / enterprise and big data solutions capability, program management skills, and agile methodology to help you develop, integrate and acquire new capabilities for emerging business functions spanning heterogeneous systems.</p>
                                
                                <p>LambdaDigital services can help customers reduce development risks, improve cost-efficiency, increase time-to-market without compromising quality in an everchanging business environment with advancements in technology. From inception, elaboration to finish, our team of software specialists will work closely with you at every step along the way to ensure that your requirements are being met within the stipulated timeframes.</p>
                                
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">ADMS includes development of new applications, features, extensions, enhancements, interfaces, and upgrades to the client's systems for existing and future business operations.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Our comprehensive set of frameworks, proven methodologies ensures the clients business applications are developed, managed and operated seamlessly to provide a secure and high performance runtime environment.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Clients benefit from our deep industry domain knowledge and technical expertise to deliver transformational results. LambdaDigital application services are designed to reduce costs and increase predictability and agility.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>We provide clients with ADM services and solutions that deliver real value. We offer a range of services from system requirements gathering, designing, development, testing and deployment, to the global management of the entire application portfolio. Throughout the project lifecycle, our focus is on the organization and providing measurable results.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--App Development  Block End-->   
                   
                        
                        
      <!--Infra. Management  Block Start-->          
                                          
         <div style="display:none;" id="Infra_mgmt"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Infra. Management</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>LambdaDigital’s IMS service offering helps you plan, design and implement a flexible, scalable, secure and responsive infrastructure to manage your organization’s mission-critical IT systems in order to help you cope up with the ever changing market and business ecosystem amidst mounting strict compliance measures.  </p>
                                
								<p>LambdaDigital leverages its in-depth industry and technical knowledge and helps clients reduce costs, accelerate time-to-market and extract maximum value from their IT investments. They provide a comprehensive IT Infrastructure management platform that helps in design, sizing, capacity planning, installation, upgrades and recovery of your organizations IT infrastructure on both cloud and virtualization platforms and on premise, that is most cost effective, reliable, and cutting edge.</p>
                                
                                <p>LambdaDigital’s key focus areas include, Maximum Infrastructure Utilization, High Availability, Reducing TCO, Increasing ROIs, Improving Operational Efficiency, Enhancing End-user experience, Cost Effectiveness and Compliance Control.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">LambdaDigital helps to manage complex environments to ensure maximum efficiency.  Modern businesses have extensive global networks, multiple partners and service providers and utilize various technologies across a range of platforms.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>LambdaDigital Infrastructure Management solution guarantees all our clients an accurate and efficient management of their IT infrastructure.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>We design, deploy and maintain network infrastructures of all sizes and ensure a good network foundation for your business. All our network infrastructure solutions are based on industry best practices and proven methodologies for Small, Medium and Large businesses.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>LambdaDigital Infrastructure Management solution ensures reduced IT costs through better uptime and prediction of potential problems on the IT infrastructure and allows efficient management of any crisis, leading to improved customer satisfaction, productivity and visibility of your entire IT infrastructure, allowing for real time diagnostics and management of multiple applications and services.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Infra. Management  Block End-->   
             
             
             
             
      
      
      
       <!--DevOps Block Start-->          
                                          
         <div style="display:none;" id="dev_ops"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>DevOps</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>LambdaDigital’s approach to Application Lifecycle Management provides a flexible and agile method that adapts to your IT team’s release cycles, LOB priorities, increase collaboration between development and operations, and streamline processes so you can focus on delivering high-quality software faster and more efficiently.  </p>
                                
								<p>With our deep experience and knowledge of DevOps, we ensure rapid on-boarding of applications by automating a CI and Development pipeline that facilitates rapid deployment and operations across leading cloud platforms.</p>
                                
                                <p>Our DevOps consulting, can help organizations achieve higher efficiency, faster recovery times and therefore stable software builds with early identification of bugs/errors, allowing lower change failure rate, and hence accelerate speed-to-market.</p>
                                <p>Our Service offering includes release management, continuous deployment, replica environment, new server setup, monitoring & control, change management and performance optimization, devops intelligence on an ongoing basis.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">DevOps is about Continuous Integration and Build Automation to get custom software through development into one’s operations as quickly as possible. One of the primary reasons for adopting DevOps best practices and automation tools is to achieve fast time to market.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>We help organizations, small and large, understand their DevOps needs and assist them to align their Development and IT Operations in order to achieve better efficiency and improve turnaround, due to changing business and technical needs.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>A DevOps strategy implemented well, provides the ability to react to change and tackle urgent challenges in the business environment. Not only does it deliver faster time to market but also increases the throughput of your software development pipeline.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>DevOps is also a powerful business enabler that helps enterprises to eliminate development and delivery constraints and successfully contain cost, mitigate risk and improve delivery cycles.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--DevOps Block End-->         
             
             
             
             
             
             
             
             
             
             
              <!--Digital Engineering Block Start-->          
                                          
         <div style="display:none;" id="test_engg"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Test Engineering</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>Testing throughout the software development lifecycle, which speeds up the application development lifecycle while bringing down overall costs, will bolster development initiatives – transforming digital initiatives.</p>

								<p>Backed by cloud capabilities, standardized delivery methodologies and higher usage of assets and frameworks, LambdaDigital uses digitaligence to transform testing into a continuous and efficient end-to-end quality engineering function to shape clients’ digital futures and lead their industry</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text"><p>LambdaDigital’s understanding of business processes, help us deliver complex testing engagements and enable our clients manage an increasingly complex technology landscape. We have deep expertise on test automation frameworks (PoS, mobile/web, ERP, legacy systems) and multiple functional automation frameworks such as Scriptless, Page-Object model, BPT and Hybrid.</p> </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-12 col-sm-12 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Framework including UI/UX, performance, security and device affinity</li>
												<li>Integrated testing platform</li>
												<li>Agile DevOps</li>
												<li>Cloud, on-premise and hybrid</li>
												<li>Multiple testing tools</li>
												<li>On-demand test environment provisioning framework</li>

                                            </ul>
                                        </div>
                                        <!--<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>-->
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>LambdaDigital strives to uphold the importance of software testing and develop high standards and professionalism in software testing, and subsequently promote awareness of industry’s best practices in software testing.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lambda Digital’s Software Testing framework provides a full range of software quality assurance services with a specialization in automation, mobility and performance testing.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>LambdaDigital’s SoftwareTesting CoE’s specializes in interpretation of a client’s needs and the development of cost effective solutions that are easily modified to suit user requirements.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Test Engineering Block End-->     
             
             
             
             
             
             
             
             
                      <!--Market Research Block Start-->          
                                          
         <div style="display:none;" id="market_research"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Market Research</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>Our team uses analytics to discover opportunities, manage risk, reduce fraud and help business focus on solving business problems that analytics can address.</p>
                                <p>We manage and automate the process of data collection, preparation and analysis, and integrate them into the operations. We provide dashboards and reports that provide appropriate and well-timed insights that allow our clients to tactical and strategic decisions that enhance shareholder value. We provide data-driven tools that enable our clients to communicate and collaborate with internal and external constituents while facilitating decision making. Our actionable insights provide clients with a way to monetize the data and transform operational performance to give significant competitive advantage. Our value lies in embedding analytics deeply into business process in a seamless manner.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Market Research Block End-->     
             
             
             
             
             
             
             
             
              
             
             
             
  
                            
                            
                            
                            
                            <!--Consult Box-->
                          <!--<div class="lower-box" >  
                            <div class="consult-box">
                            	<div class="clearfix">
                                	<div class="column pull-left">
                                    	<h3>We Have Great Advisor!!</h3>
                                        <div class="text">Here is something that is related with <span class="theme_color">technology & business</span> consulting services</div>
                                    </div>
                                    <div class="column pull-right">
                                    	<a href="./contact.php" class="theme-btn btn-style-eight">Free Consultation</a>
                                    </div>
                                </div>
                            </div>-->
                            <!--End Consult Box-->
                            
                            
                            
                            <!--div class="accordian-boxed">
                            	<h2>Frequently Asked Question</h2><br />
                            	<Accordion Box>
                                <ul class="accordion-box">
                                
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How i get the free consulting and start with advisor?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How investment plan work for us?</div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How can i make profit with investmant plan? </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                </ul>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
  <?php   include_once "footer.php";    ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script type="text/javascript">

function show(divId) {
	$("#App_develop").hide();
	$("#Infra_mgmt").hide();
	$("#dev_ops").hide();
	$("#test_engg").hide();
	$("#market_research").hide();
	$("#"+divId).show();
}

function display(divId, leftMenuId) {
	$("#pg_tl_App_develop").hide();
	$("#pg_tl_Infra_mgmt").hide();
	$("#pg_tl_dev_ops").hide();
	$("#pg_tl_test_engg").hide();
	$("#pg_tl_market_research").hide();
	
	$("#left_menu_adms").removeClass("current");
	$("#left_menu_infra_manag").removeClass("current");
	$("#left_menu_devop_eng").removeClass("current");
	$("#left_menu_test_eng").removeClass("current");
	$("#left_menu_pg_tl").removeClass("current");
	 
	$("#"+leftMenuId).addClass("current");
	$("#"+divId).show();
}
</script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
