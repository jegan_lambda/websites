<?php 
// Connect to the database
include('config.php'); 
$id_post = "1"; //the post or the page id
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - A technology & business consulting company | Blog Single</title>
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>



<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
 	 <?php   include_once "header.php";    ?>
    <!--End Main Header -->
    
	<!--Page Title-->
    <section class="page-title" style="background-image:url(./images/background/3.jpg);">
        <div class="auto-container">
            <h1>Blog Details</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.html">Home</a></li>
                    <li>News</li>
                </ul>
            </div>
            <div class="pull-right">
                <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<div class="blog-single">
						<!--News Style Three-->
                        <div class="news-style-three">
                            <div class="inner-box">
                                <div class="image">
                                    <img src="./images/resource/news-9.jpg" alt="" />
                                </div>
                                <div class="lower-box">
                                    <div class="date">13 Feb, 2016  /  Business</div>
                                    <h3>How to become a best sale marketer in a year!</h3>
                                    <div class="text">
                                    	<p>Bar none, the biggest objection a customer ever raises is price. Often they don’t comprehend the value of your solution, therefore concluding that the number you’ve quoted is completely arbitra, may even greed-based. It’s true that today’s customers exist in a climate of global competition; they know how to </p>
                                        <h4>Real World Example</h4>
                                        <p>Bar none, the biggest objection a customer ever raises is price. Often they don’t comprehend the value of your solution, therefore concluding that the number you’ve quoted is completely arbitrary,</p>
                                        <div class="two-column row clearfix">
                                        	<div class="column col-md-5 col-sm-6 col-xs-12">
                                            	<div class="image">
                                                	<img src="./images/resource/blog-single.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="column col-md-7 col-sm-6 col-xs-12">
                                            	<p>Bar none, the biggest objection a customer ever raises is price. Often they don’t comprehend.</p>
                                                <p>solution, therefore concluding that the number you’ve quoted is completely arbitrary, maybe even greed-based. It’s true that today’s customers exist in a climate of global competition; they know how to </p>
                                            </div>
                                        </div>
                                        <p>Bar none, the biggest objection a customer ever raises is price. Often they don’t compreh value of your solution, therefore concluding that the number you’ve quoted is completely arbitrary, maybe even best greed-based. It’s true that today’s customers exist in a climate of global competition.</p>
                                    </div>
                                </div>
                                <!--post-share-options-->
                                <div class="post-share-options clearfix">
                                    <div class="pull-left tags"><a href="#">Business</a> <a href="#">Finance</a></div>
                                    <div class="pull-right social-icon-three clearfix">
                                        <div class="share">Share</div>
                                        <a href="#" class="facebook"><span class="fa fa-facebook-f"></span></a>
                                        <a href="#" class="twitter"><span class="fa fa-twitter"></span></a>
                                        <a href="#" class="linkedin"><span class="fa fa-google-plus"></span></a>
                                        <a href="#" class="google-plus"><span class="fa fa-linkedin"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
            
                        
                        
                        
                        
                        <!--Comments Area-->
                        <div class="comments-area">
                            
                            
                            
                            
                             <?php 
							$sql = mysql_query("SELECT * FROM comments WHERE id_post = '$id_post'") or die(mysql_error());;
							
							$num_rows = mysql_num_rows($sql);
							?>
                            <div class="group-title"><h2> <?php echo $num_rows; ?> &nbsp;  Comments</h2></div>
                            <?php
							while($affcom = mysql_fetch_assoc($sql)){ 
								$name = $affcom['name'];
								$email = $affcom['email'];
								$comment = $affcom['comment'];
								$date = $affcom['date'];
						
								// Get gravatar Image 
								// https://fr.gravatar.com/site/implement/images/php/
								$default = "mm";
								$size = 35;
								$grav_url = "http://www.gravatar.com/avatar/".md5(strtolower(trim($email)))."?d=".$default."&s=".$size;
						
							?>
      						  <!--Comment Box-->
                            <div class="comment-box">
                                <div class="comment">
                                    <div class="author-thumb"><img src="./images/resource/author-3.jpg" alt="" /></div>
                                    <div class="comment-inner">
                                        <div class="comment-info clearfix"><strong><?php echo $name; ?></strong><div class="comment-time"><?php echo $date; ?></div></div>
                                        <div class="text"> <?php echo $comment; ?> </div>
                                     <!--   <a class="comment-reply" href="#">Reply</a>-->
                                    </div>
                                </div>
                            </div>
                          
   						 <?php } ?>
                        
                           
                        </div>
                        
                    

                        
                        

						<!-- Comment Form -->
                        <div class="comment-form">
                                
                            <div class="group-title"><h2>Leave a Reply</h2></div>
                            <div class="group-sub-title">Your must sing-in to make or comment a post</div>
                            <!--Comment Form-->
                            <form method="post"  />
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="text"  id="name-com" name="username" placeholder="Name *" />
                                        <div id="error" style="display:none;">Enter Value...</div>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="email" id="mail-com" name="email" placeholder="Email *"  />
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" class="the-new-com"  placeholder="Comment *"></textarea>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-two bt-add-com" id="btnValidate"  >Post Comment</button>
                                    </div>
                                    
                                </div>
                            </form>
                                
                        </div>
                        <!--End Comment Form --> 

                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-8 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Search -->
                        <div class="sidebar-widget search-box">
                        	<form method="post" action="contact.html" />
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search Here.." />
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
						</div>
                                
                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title">
                                <h2>Categories</h2>
                            </div>
                            <ul class="blog-cat">
                                <li><a href="#">Budget</a></li>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Invesment</a></li>
                                <li><a href="#">Stock</a></li>
                                <li><a href="#">Financial</a></li>
                            </ul>
                        </div>
                                
                        
                        <!-- Popular Posts -->
                         <?php   include_once "sidebar_technews.php";    ?>
                      
                        
                               
                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h2>Keyword</h2></div>
                            <a href="#">Advise</a>
                            <a href="#">Business</a>
                            <a href="#">Marketing</a>
                            <a href="#">Financial</a>
                            <a href="#">planning</a>
                            <a href="#">consulting</a>
                        </div>
                                                
                    </aside>
                </div>
                
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
 <?php   include_once "footer.php";    ?>

<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
<script type="text/javascript">

$(document).ready(function(){
				
												 
			$('#btnValidate').click(function(){
            var theCom = $('.the-new-com');
            var theName = $('#name-com');
            var theMail = $('#mail-com');

            if( !theCom.val()  || !theCom.val() || !theMail.val()){ 
                alert('You need to Fill all the Details'); 
				return false;
            }else{ 
                $.ajax({
                    type: "POST",
                    url: "ajax/add-comment.php",
                    data: 'act=add-com&id_post='+<?php echo $id_post; ?>+'&name='+theName.val()+'&email='+theMail.val()+'&comment='+theCom.val(),
                    success: function(html){
                        theCom.val('');
                        theMail.val('');
                        theName.val('');
                      
					  alert("Your Comment Was Sucessfully Registered, Thank You!");
					  return false;
					
                    }  
                });
            }
        });							 
												 
					
												
		
      		 
   
});
						   
  
</script>


</body>
</html>
