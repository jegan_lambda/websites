<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LambdaDigital - Product Engineering</title>
<meta name="description" content="LambdaDigital Private Limited is a Machine Learning solutions provider, AI and BI solutions provider, Product development company, Block Chain solutions provider" />
<meta name="keywords" content="LambdaDigital, Lambdadigital, lambda digital, LambdaDigital Private Limited, Block Chain development company in india, Machine Learning development company in india, AI development company in india, BI development company in india" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true" />
<meta name="apple-touch-fullscreen" content="yes" />
<!-- Stylesheets -->
<link href="./css/bootstrap.css" rel="stylesheet" />
<link href="./css/style.css" rel="stylesheet" />
<!--Favicon-->
<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="./css/responsive.css" rel="stylesheet" />
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="./js/respond.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
    	<?php   include_once "header.php"; ?>
    <!--End Main Header -->
    
	<!--Page Title-->

 	<div id="pg_tl_product_engg"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Product Engineering</h1>
			</div>
		</section>
	</div>	
	
	<div style="display:none;" id="pg_tl_research_development"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Research & Development</h1>
			</div>
		</section>
	</div>
	
 	<div style="display:none;" id="pg_tl_rapid_prototyping"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Rapid Prototyping</h1>
			</div>
		</section>
	</div>
	
 	<div style="display:none;" id="pg_tl_agile_methods"> 
		<section class="page-title" style="background-image:url(./images/background/3.jpg);">
			<div class="auto-container">
				<h1>Agile Methods</h1>
			</div>
		</section>
	</div>
	<!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container clearfix">
            <div class="pull-left">
            	<ul class="bread-crumb clearfix">
                    <li><a href="./index.php">Home</a></li>
					<li>Services</li>
                    <li>Product Development</li>
                </ul>
            </div>
            <div class="pull-right">
               <!-- <div class="share-icon"><a href="#"><span class="icon fa fa-share"></span> Share</a></div>-->
            </div>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar">
						
                        <!-- Sidebar Category -->
                        <div class="sidebar-widget sidebar-category">
                            <ul class="list">
                                <li class="current" id="left_menu_prod_engi"><a href="javascript:show('product_engg'); javascript:display('pg_tl_product_engg','left_menu_prod_engi');">Product Engineering</a></li>
                                <li id="left_menu_research_develop"><a href="javascript:show('research_development'); javascript:display('pg_tl_research_development','left_menu_research_develop');">Research & Development</a></li>
                                <li id="left_menu_rapid_prototyping"><a href="javascript:show('rapid_prototyping'); javascript:display('pg_tl_rapid_prototyping','left_menu_rapid_prototyping');">Rapid Prototyping</a></li>
                                <li id="left_menu_agile_method"><a href="javascript:show('agile_methods'); javascript:display('pg_tl_agile_methods','left_menu_agile_method');">Agile Methods</a></li>

                            </ul>
                        </div>
                        
                        <!-- Sidebar Testimonial -->
                       <!-- <div class="sidebar-widget sidebar-testimonial">
                        	<div class="single-item-carousel owl-carousel owl-theme">
                            	<!--Testimonial Slide-->
                            <!--	<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>-->
                                
                                <!--Testimonial Slide-->
                            	<!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>-->
                                
                                <!--Testimonial Slide-->
                            	 <!--<div class="testimonial-slide">
                                	<div class="inner">
                                    	<div class="author-info">
                                        	<div class="image">
                                            	<img src="./images/resource/author-2.jpg" alt="" />
                                            </div>
                                            <h3>Jasmin Rose</h3>
                                            <div class="designation">Founder of Innovate</div>
                                        </div>
                                        <div class="text">Leverage agile framework to the provide a robust synoce for high level overviews.</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>-->
                        
                        <!-- Sidebar Question Box -->
                        <div class="sidebar-widget sidebar-question">
                        	<div class="info-widget">
                                <div class="inner">
                                    <h3>Have Any Question <br /> Call Us Now</h3>
                                    <h2>+91 9840 358 348</h2>
                                    <a href="#" class="more-detail">More Details</a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Sidebar brochure-->
                        <div class="sidebar-widget sidebar-brochure">
                        	  <!--<a class="brochure" href="#"><span class="icon flaticon-pdf"></span> Details Brochure.pdf <span class="download-icon flaticon-download-arrow-1"></span></a>-->
                        </div>
                        
                    </aside>
                </div>
                
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<!--Services Single-->
                	<div class="services-single">
						<div class="services-single-images">
                        	<div class="row clearfix">
                            	<div class="column col-md-8 col-sm-8 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-1.jpg" title="Image Caption Here"><img src="./images/resource/services-single-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                                <div class="column col-md-4 col-sm-4 col-xs-12">
                                	<div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-2.jpg" title="Image Caption Here"><img src="./images/resource/services-single-2.jpg" alt="" /></a>
                                    </div>
                                    <div class="image">
                                    	<a class="lightbox-image" href="./images/resource/services-single-3.jpg" title="Image Caption Here"><img src="./images/resource/services-single-3.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
             <!--Product Engineering Block Start-->          
                                          
         <div  id="product_engg"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Product Engineering</h3>
                            <div class="sub-title">LambdaDigital provides end-to-end offshore product development services to  <span class="theme_color">global ISVs</span>, <span class="theme_color">small</span> and<span class="theme_color"> medium enterprises</span>.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>We combine a state-of-the-art lab, a sophisticated technical team that specializes on OSS frameworks & trending technologies and a hybrid engagement model that can translate your idea into a market-ready product at a minimal cost while ensuring IP rights are protected.</p>
                                <p>Outsourcing Product Development means, companies can focus on core activities like market analysis, marketing, and business development.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">LambdaDigital guarantees knowledge retention through dedicated project teams thereby reducing overall cost of software product development dramatically.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Product Conceptualization</li>
                                                <li>Architecture Design</li>
						<li>UI/UX Development</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Development & Testing</li>
                                                <li>Documentation</li>
						<li>Maintenance & Support</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Product Development Services</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Product Development - Full Lifecycle</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>We’ll help you choose technologies, setup the right architecture and leverage emerging tools and trends.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Product Customization</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Develop multiple versions of products, or include greater functionality at tighter deadlines.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Product Component Development</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Dedicated offshore Development center to enabling customers focus on their core business.</p></div>
                                            </div>
                                        </li>

                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">4</span> </div>Joint Product Development & Support</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Reduced cycle times, lower costs, higher quality, and rapid resource ramp up/down are key features.</p></div>
                                            </div>
                                        </li>

                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">5</span> </div>Product Maintenance & Support</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Dedicated Offshore Development Center to enable Customers focus on their core business.</p></div>
                                            </div>
                                        </li>                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Product Engineering  Block End-->   
                      
                        
      <!--Research & Development Block Start-->          
                                          
         <div style="display:none;" id="research_development"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Research & Development</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>In R&D we bring ideas to life. We look to the future, take the lead, move fast and stay ahead of the competition.</p>
                                <p> Our service of research and development outsourcing is dedicated to small and medium-sized businesses that cannot sustain all the costs and risks of research and development of innovative/competitive solutions, as well as for large companies to support and complete internal resources, in the context of 'Open Innovation'.</p>

								<p>Innovation is the competitive advantage for a global market, integrating into the product and the company’s offerings, new materials, processes and methodologies. LambdaDigital presents specialists and professionals that regularly collaborate with university research centers active in international arenas, providing an ample view of adoptable forefront solutions and technologies. Capitalizing on this resource provides the benefits of a combined outlook and fosters continual improvement. In doing so, a focused strategy can be planned, increasing the possibility of success.</p>

								<p>Our experts are able to provide support and expertise in the most cutting edge technologies ensuring compliance with international standards, and evaluating environmental and economic sustainability of the products and processes developed.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">At LambdaDigital R&D Labs we bring ideas to life. We look to the future, take the lead, move fast and stay ahead of the competition. We use in-depth consumer understanding and expertise in science and technology to create innovative, cost effective and sustainable products and solutions.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
						<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
						<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Our engineers and scientists work closely with your business development team to creäte innovative concepts and ideas, exploiting both market needs.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Discover and deliver functional benefits through new product technology. Even as we focus on developing new technologies, we continuously monitor research efficiency and focus on monetizing research efforts.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>We provide technical support in the design, implementation, analysis, and interpretation of market and social trends, on a project to project basis, with the aim of delivering quality research output to clients.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Research & Development  Block End-->   
      
       <!--Rapid Prototyping Block Start-->          
                                          
         <div style="display:none;" id="rapid_prototyping"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Rapid Prototyping</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>DESIGN-PROTOTYPE-VALIDATE.</p>
                                <p>Going to market quickly is more than important, it’s essential. Functional prototyping allows you to test the performance of your ideas and make necessary refinements before committing to full production.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Lambdadigital facilitates your entire product development process, from problem identification, to concept formulation, ideation, whiteboarding & developing prototypes and pilots. We focus on bringing our clients’ ideas to life.</div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Development dependent, we go through various stages of product prototyping both during the conceptual design and detailed design phase through various technical PoC’s (Proof-of-Concepts).</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>We rapidly prototype to establish the capability of the suggested architecture to deliver stated and implied needs. This is the stage where the operational, usability, extensibility and performance characteristics are further studied to refine and lead to a baselined architecture.</p></div>
                                            </div>
                                        </li>

                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lambdadigital final prototypes are always 100% functional and testable.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Rapid Prototyping Block End-->         
             
              <!--Digital Engineering Block Start-->          
                                          
         <div style="display:none;" id="agile_methods"> 
                        <!--Upper Box-->
                        <div class="upper-box" >
                        	<h3>Agile Methods</h3>
                            <div class="sub-title">We’ll be with you on every walk of life to <span class="theme_color">identify new</span> opportunities.</div>
                        </div>
                        <div class="lower-box"  >
                        	<div class="text">
                            	<p>Our team uses analytics to discover opportunities, manage risk, reduce fraud and help business focus on solving business problems that analytics can address.</p>
                                <p>We manage and automate the process of data collection, preparation and analysis, and integrate them into the operations. We provide dashboards and reports that provide appropriate and well-timed insights that allow our clients to tactical and strategic decisions that enhance shareholder value. We provide data-driven tools that enable our clients to communicate and collaborate with internal and external constituents while facilitating decision making. Our actionable insights provide clients with a way to monetize the data and transform operational performance to give significant competitive advantage. Our value lies in embedding analytics deeply into business process in a seamless manner.</p>
                            </div>
                            
                            <div class="row clearfix">
                            	<!--Column-->
                            	<div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Challenging Part</h2>
                                    <div class="text">Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia. Et iudibit theophrastus signiferumque vis. </div>
                                    <div class="bold-text">We carefully complete every challenging task!!</div>
                                    <div class="row clearfix">
                                    	<div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Understand Problem</li>
                                                <li>Data Collection</li>
												<li>Features Engineering</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        	<ul class="list-style-two">
                                            	<li>Build Model</li>
                                                <li>Evaluate</li>
												<li>Operationalize Model</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Column-->
                                <div class="column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Our Work Strategies</h2>
                                    
									<!--Accordion Box-->
                                    <ul class="accordion-box style-two">
                                    
                                        <!--Block-->
                                        <li class="accordion block active-block">
                                            <div class="acc-btn active"><div class="icon-outer"><span class="icon">1</span> </div>Getting Ready for the Project</div>
                                            <div class="acc-content current">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">2</span> </div>Research the Market</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                        <!--Block-->
                                        <li class="accordion block">
                                            <div class="acc-btn"><div class="icon-outer"><span class="icon">3</span> </div>Make the Final Output</div>
                                            <div class="acc-content">
                                                <div class="content-text"><p>Lorem ipsum dolor sit amet, vix an natum labitur eleifend, mel amet a laorit menandri. Ei justo complectitur duo. Soletu mel possit quo.</p></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <!--End Accordion Box-->
                                    
                                </div>
                            </div>
                            </div>
                </div>            
             <!--Agile Methods Block End-->     
                            
                            <!--Consult Box-->
                          <!--<div class="lower-box" >  
                            <div class="consult-box">
                            	<div class="clearfix">
                                	<div class="column pull-left">
                                    	<h3>We Have Great Advisor!!</h3>
                                        <div class="text">Here is something that is related with <span class="theme_color">technology & business</span> consulting services</div>
                                    </div>
                                    <div class="column pull-right">
                                    	<a href="./contact.php" class="theme-btn btn-style-eight">Free Consultation</a>
                                    </div>
                                </div>
                            </div>-->
                            <!--End Consult Box-->
                            
                            
                            
                            <!--div class="accordian-boxed">
                            	<h2>Frequently Asked Question</h2><br />
                            	<Accordion Box>
                                <ul class="accordion-box">
                                
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How i get the free consulting and start with advisor?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How investment plan work for us?</div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                    <Block>
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>How can i make profit with investmant plan? </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                	<p>We also ensure that the whole team is included in process and that no one is left out during the turnaround. The most crucial part  ensuring some degree financialstability during the turnaround.</p>
                                                    <p>Nemore tincidunt ea mel, eos cu alii insolens signiferumque. Te nec dolor clita partem, mea ne iuvaret aliquid. Id nam illum augue commodo, diam dolores philosophia nec.Ne stet adolescens efficiendi te pri. Et nec natum inciderint, eos ex gubergren  Nemore tincidunt ea mel, eos cu alii insolens signiferumque. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
            
                                </ul>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
   	</div>
    
    <!--Main Footer-->
<?php   include_once "footer.php"; ?>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="./js/jquery.js"></script> 
<script type="text/javascript">

function show(divId) {
	$("#product_engg").hide();
	$("#research_development").hide();
	$("#rapid_prototyping").hide();
	$("#agile_methods").hide();
	$("#"+divId).show();
}

function display(divId,leftMenuId) {
	$("#pg_tl_product_engg").hide();
	$("#pg_tl_research_development").hide();
	$("#pg_tl_rapid_prototyping").hide();
	$("#pg_tl_agile_methods").hide();	
	
	$("#left_menu_prod_engi").removeClass("current");
	$("#left_menu_research_develop").removeClass("current");	
	$("#left_menu_rapid_prototyping").removeClass("current");
	$("#left_menu_agile_method").removeClass("current");	 
	 
	$("#"+leftMenuId).addClass("current");
	$("#"+divId).show();
}

</script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.fancybox.pack.js"></script>
<script src="./js/jquery.fancybox-media.js"></script>
<script src="./js/owl.js"></script>
<script src="./js/appear.js"></script>
<script src="./js/wow.js"></script>
<script src="./js/script.js"></script>
</body>
</html>
